#!/bin/sh

hostnameprefix=${hostname.prefix}
hostnamelist=( ${hostname.list} )
totalhosts=${total.hosts}

GRIDVISE_HOME=${gridvise.home}/${pom.artifactId}-${pom.version}
starthost=${start.host}

j=0
[ "$hostnameprefix" == "" ] && totalhosts=${#hostnamelist[@]}

SCRIPT_NAME=$1

thishost=`hostname`
for ((i=1;i<=$totalhosts;i+=1))
do
        if [ "$hostnameprefix" == "" ]
        then
                currenthost=${hostnamelist[${j}]}
                j=$(($j + 1))
        else
                currenthost=$hostnameprefix`printf "%02d" $starthost`
        fi
        starthost=$(($starthost + 1))

        if [ "$currenthost" == "$thishost" ]
        then
                $GRIDVISE_HOME/bin/${SCRIPT_NAME} &
        else
                ssh $currenthost $GRIDVISE_HOME/bin/${SCRIPT_NAME}
        fi
done
