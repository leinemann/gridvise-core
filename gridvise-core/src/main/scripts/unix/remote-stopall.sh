#!/bin/sh

if [ $# -lt "1" ]
then
        echo "Do you really want to stop the cluster?"
        read response
else
        response=$2
fi

if [ $response != 'y' ]; then
        echo "reply y to confirm, operation aborted"
        exit 1
fi

#
# --- DO NOT EDIT BELOW THIS POINT ---
#
hostnameprefix=${hostname.prefix}
hostnamelist=( ${hostname.list} )
totalhosts=${total.hosts}
GRIDVISE_HOME=${gridvise.home}/${pom.artifactId}-${pom.version}
stoparg=y
starthost=${start.host}
j=0
[ "$hostnameprefix" == "" ] && totalhosts=${#hostnamelist[@]}

thishost=`hostname`
for ((i=1;i<=$totalhosts;i+=1))
do
        if [ "$hostnameprefix" == "" ]
        then
                currenthost=${hostnamelist[${j}]}
                j=$(($j + 1))
        else
                currenthost=$hostnameprefix`printf "%02d" $starthost`
        fi
        starthost=$(($starthost + 1))
        if [ "$currenthost" == "$thishost" ]
        then
                echo executing stopall.sh on $currenthost
                $GRIDVISE_HOME/bin/stopall.sh $stoparg
        else
                echo executing stopall.sh on $currenthost
                ssh $currenthost $GRIDVISE_HOME/bin/stopall.sh $stoparg
        fi
        echo --------------------------------------------------------------------------------------
done
        