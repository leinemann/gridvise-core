#!/bin/sh

#
# --- DO NOT EDIT BELOW THIS POINT ---
#

function arrayContains()
{
        EXPECTED_ARGS=2
        retval=0

        # Setting the shell's Internal Field Separator to null
        OLD_IFS=$IFS
        IFS=''

        #check if parameter exists, else return 0
        if [ $# -ne $EXPECTED_ARGS ]
        then
                echo "at least $# arguments expected for function arrayContains"
                exit -1
        fi

        # Create a string containing passed in values
        local arrstr="$1[*]"
        # assign loc_array value to above string using indirect variable reference

        # Resetting IFS to default
        IFS=$OLD_IFS

        for i in ${!arrstr}
        do
                if [ "$i" == "$2" ]
                then
                        retval=1
                fi
        done
        echo $retval
        return
}


hostnameprefix=${hostname.prefix}
hostnamelist=( ${hostname.list} )
totalhosts=${total.hosts}
nostoragenodegap=${nostorage.node.gap}
GRIDVISE_HOME=${gridvise.home}/${pom.artifactId}-${pom.version}
starthost=${start.host}
aggregationarray=( ${aggrproc.server.list} )
parallelaggrarray=( ${parallel.aggrproc.server.list} )
excludestoragearray=( ${exclude.storage.server.list} )
loadtaskarray=( ${loadtask.server.list} )
bczeroarray=( ${bczero.server.list} )
j=0
[ "$hostnameprefix" == "" ] && totalhosts=${#hostnamelist[@]}

thishost=`hostname`
for ((i=1;i<=$totalhosts;i+=1))
do
        nostoragearg="-nostorage n"
        clusterarg="-cluster n"
        jmxarg="-jmx n"
        if [ "$hostnameprefix" == "" ]
        then
                currenthost=${hostnamelist[${j}]}
                j=$(($j + 1))
        else
                currenthost=$hostnameprefix`printf "%02d" $starthost`
        fi
        aggrprocarg="-aggrproc n"        
        bczeroarg="-bczero n"        
        parallelaggrprocgaparg="-parallelaggrproc n"        
        loadtaskarg="-loadtask n"        
        starthost=$(($starthost + 1))
        #
        # --- start cluster & nostorage service on last server
        #
        [ $i -eq $totalhosts ] && clusterarg="-cluster y"
        [ $i -eq $totalhosts ] && nostoragearg="-nostorage y"
        #[ $i -eq $totalhosts ] && jmxarg="-jmx y"
        #
        # --- start aggregations processing on selected servers determined by list
        #
        aggrproc=$(arrayContains aggregationarray $i)
        if [ $aggrproc == "1" ]
        then
        	aggrprocarg="-aggrproc y"
        fi

        loadtask=$(arrayContains loadtaskarray $i)
        if [ $loadtask == "1" ]
        then
        	loadtaskarg="-loadtask y"
        fi
        #
        # --- start BC0 processing on selected servers determined by list
        #
        bczero=$(arrayContains bczeroarray $i)
        if [ $bczero == "1" ]
        then
                bczeroarg="-bczero y"
        fi

        excludestorageproc=$(arrayContains excludestoragearray $i)
        if [ $excludestorageproc == "1" ]
        then
                storagearg="-storage n"
        else
                storagearg="-storage y"
        fi

        #
        # --- start paralle aggregations processing on selected servers determined by list
        #
        parallelaggrproc=$(arrayContains parallelaggrarray $i)
        if [ $parallelaggrproc == "1" ]
        then
        	parallelaggrprocgaparg="-parallelaggrproc y"
        fi
        #
        # --- use ssh only if server is not this server
        #
        if [ "$currenthost" == "$thishost" ]
        then
                echo executing startall.sh $storagearg $clusterarg $nostoragearg $jmxarg $aggrprocarg $loadtaskarg $parallelaggrprocgaparg $bczeroarg on $currenthost
                $GRIDVISE_HOME/bin/startall.sh $storagearg $clusterarg $nostoragearg $jmxarg $aggrprocarg $loadtaskarg $parallelaggrprocgaparg $bczeroarg
        else
                echo executing startall.sh $storagearg $clusterarg $nostoragearg $jmxarg $aggrprocarg $loadtaskarg $parallelaggrprocgaparg $bczeroarg on $currenthost
                ssh $currenthost $GRIDVISE_HOME/bin/startall.sh $storagearg $clusterarg $nostoragearg $jmxarg $aggrprocarg $loadtaskarg $parallelaggrprocgaparg $bczeroarg
        fi
        echo --------------------------------------------------------------------------------------
        #echo "Press any key to continue (or 'exit' to stop)"
        #read response
        #if [ "$response" == "exit" ]; then
        #        echo exiting script
        #        exit -1
        #fi
done
                        
        