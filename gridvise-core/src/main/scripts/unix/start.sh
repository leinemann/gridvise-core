#!/bin/sh

#//TODO - check if gridvise home is set
GRIDVISE_HOME=`pwd`
HOSTNAME=`hostname`

echo "using home dir $GRIDVISE_HOME on host $HOSTNAME"

export GRIDVISE_HOME=$GRIDVISE_HOME

#Load jdk

CLASSPATH=$GRIDVISE_HOME/conf:$GRIDVISE_HOME/lib/*;

export CLASSPATH

echo "using classpath: $CLASSPATH"

#JAVA_OPTS_REMOTE_DEBUG="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=10045 "
JAVA_OPTS_REMOTE_DEBUG=

JAVA_OPTS="-Dtangosol.coherence.mode=dev -XX:+HeapDumpOnOutOfMemoryError -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+PrintGCDetails  -Dcom.sun.management.jmxremote -Dtangosol.coherence.management.remote=true"

#//TODO check if logs exist?
mkdir $GRIDVISE_HOME/logs

LOG=$GRIDVISE_HOME/logs/gridcontrol-$HOSTNAME.stderr.log

EXEC_CMD="java -Djava.net.preferIPv4Stack -Xmx1024m -Xms1024m $JAVA_OPTS_REMOTE_DEBUG -Dlog4j.application.name=$HOSTNAME.gridvise -Dtangosol.coherence.member=$HOSTNAME -Dhostname=$HOSTNAME -Dtangosol.coherence.log.level=6 org.gridvise.mgmtcache.coh.ManagementCacheServer"


echo $EXEC_CMD
$EXEC_CMD  >> $LOG  2>&1 &
PID=$!
echo "$PID" > $GRIDVISE_HOME/bin/$HOSTNAME.$1.$2