package org.gridvise

import org.gridvise.mgmtcache.coh.entity.executioncontext.{ExecutionContext, ExecutionContextFactory}
import org.gridvise.mgmtcache.coh.invocation.tasks.runbook.{RunBookLoadTask, RunBookTask, RunActivityTask}
import org.gridvise.mgmtcache.coh.invocation.ExtendInvocationService
import scala.collection.JavaConversions._


object RemoteRunBookAPI {

  val defaultExecutionContext = ExecutionContextFactory.apply(RemoteAPI.machineNames())

  def invokeRunBookActivity(runBookId: String, activityId: String, vars: Map[String, String]): String = {
    invokeRunBookActivity(defaultExecutionContext, runBookId,activityId, vars)
  }

  def invokeRunBookActivity(executionContext: ExecutionContext, runBookId: String, activityId: String, vars: Map[String, String]): String = {
    val r = new RunActivityTask(runBookId, activityId)
    r.setVars(vars)
    r.setExcecutionContext(executionContext)
    ExtendInvocationService.queryOnAllMembers(r).toString
  }

  def invokeRunBook(runBookId: String, vars: Map[String, String]): String =  {
    invokeRunBook(defaultExecutionContext, runBookId, vars)
  }

  def invokeRunBook(runBookId: String, vars: java.util.Map[String, String]): String =  {
    invokeRunBook(defaultExecutionContext, runBookId, vars.toMap[String, String])
  }

  def invokeRunBook(executionContext: ExecutionContext, runBookId: String, vars: Map[String, String]): String =  {
    val r = new RunBookTask(runBookId)
    r.setVars(vars)
    r.setExcecutionContext(executionContext)
    ExtendInvocationService.queryOnAllMembers(r).toString
  }

  def invokeRunBook(executionContext: ExecutionContext, runBookId: String, vars: java.util.Map[String, String]): String =  {
    invokeRunBook(executionContext, runBookId, vars.toMap)
  }


  def loadRunBook(executionContext: ExecutionContext, runBookLocation : String): String = {
    val r = new RunBookLoadTask(runBookLocation)
    r.setExcecutionContext(executionContext)
    ExtendInvocationService.queryOnAllMembers(r).toString
  }

}
