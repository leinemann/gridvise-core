package org.gridvise
import org.gridvise.logical.ClusterConfigParser
import org.gridvise.mgmtcache.coh.entity.config.ConfigCache
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableCache

object LocalAPI {

  def start() {
    LaunchableCache.getLaunchablesOnThisMachine().foreach(l => l.start() )
  }

  def stop() {
    LaunchableCache.getLaunchablesOnThisMachine().foreach(l => l.stop())
  }

  def initialize() {
    ClusterConfigParser.initialize(ConfigCache.getConfig())
  }

}