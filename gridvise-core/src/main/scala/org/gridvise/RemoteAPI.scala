package org.gridvise
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import org.gridvise.logical.ThreadDump
import org.gridvise.mgmtcache.coh.entity.config.ConfigCache
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableCache
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey
import org.gridvise.mgmtcache.coh.entity.launchable.RunningState
import org.gridvise.mgmtcache.coh.entity.logging.LoggingCache
import org.gridvise.mgmtcache.coh.invocation.tasks._
import org.gridvise.mgmtcache.coh.invocation.ExtendInvocationService
import org.slf4j.LoggerFactory
import scala.xml.XML
import org.gridvise.logical.os.{ OSOperations, MachineInfo }
import org.gridvise.mgmtcache.coh.entity.executioncontext.ExecutionContextFactory
import org.gridvise.mgmtcache.coh.entity.executioncontext.ExecutionContext
import java.util.Collection

object RemoteAPI {

  val LOG = LoggerFactory.getLogger(this.getClass())

  def start(launchableKeys: java.util.Collection[LaunchableKey]): String = {
    val startTask = new RemoteStartTask()
    startTask.setKeys(launchableKeys.toList)
    val launchableSet = ExtendInvocationService.queryOnAllMembers(startTask)
    launchableSet.toList.foreach(s => LOG.info(s))
    "started " + launchableSet.size() + " jvms"
  }

  def startJvmGroup(jvmGroup: String) {
    val startTask = new RemoteStartTask()
    val keys = LaunchableCache.getLaunchableKeysForNodeGroup(jvmGroup)
    if (keys.size == 0) LOG.info("no nodes associated with nodegroup " + jvmGroup)
    else {
      startTask.setKeys(keys)
      (ExtendInvocationService.queryOnAllMembers(startTask)).toList.foreach(s => LOG.info(s))
    }
  }

  def start() {
    val startTask = new RemoteStartTask()
    (ExtendInvocationService.queryOnAllMembers(startTask)).toList.foreach(s => LOG.info(s))
  }

  def start(jvmConfig: String): String = {
    start(LaunchableCache.getLaunchablesForJvmConfig(jvmConfig))
  }

  def getRunningState(launchableKeys: java.util.Collection[LaunchableKey]): RunningState.Value = {
    LaunchableCache.getRunningState(launchableKeys.toList)
  }

  def stop() {
    ExtendInvocationService.queryOnAllMembers(new RemoteStopTask()).toList.foreach(s => LOG.info(s))
  }

  def stop(jvmConfig: String): String = {
    stop(LaunchableCache.getLaunchablesForJvmConfig(jvmConfig))
  }

  def stop(launchableKeys: java.util.Collection[LaunchableKey]): String = {
    val stopTask = new RemoteStopTask()
    stopTask.setKeys(launchableKeys.toList)
    val launchableSet = ExtendInvocationService.queryOnAllMembers(stopTask)
    launchableSet.toList.foreach(s => LOG.info(s))
    "stopped " + launchableSet.size() + " jvms"
  }

  def threadDump(launchableKeys: java.util.Collection[LaunchableKey]): List[ThreadDump] = {
    val threadDumpTask = new RemoteThreadDumpTask()
    threadDumpTask.setKeys(launchableKeys.toList)
    val dumpList = ExtendInvocationService.queryOnAllMembers(threadDumpTask)
    var returnList: List[ThreadDump] = Nil
    dumpList.foreach(l => l.foreach(t => returnList ::= t))
    returnList
  }

  def exec(cmd: String): List[String] = {
    val cmdTask = new RemoteCmdTask(cmd)
    ExtendInvocationService.queryOnAllMembers(cmdTask).asScala.toList
  }

  def exec(cmd: String, host: String): List[String] = {
    val cmdTask = new RemoteCmdTask(cmd, host)
    ExtendInvocationService.queryOnAllMembers(cmdTask).asScala.toList
  }

  def exec(cmds: List[String]): List[String] = {
    var rList = List[String]()
    cmds.foreach(cmd => {
       rList = rList ++ exec(cmd)
    })
    rList
  }
  
  def exec(cmd: String, hosts: List[String]): List[String] = {
    val cmdTask = new RemoteCmdTask(cmd, hosts)
    ExtendInvocationService.queryOnAllMembers(cmdTask).asScala.toList
  }

  def exec(cmd: String, hosts: List[String], pauseSeconds: Integer): List[String] =  {
    var rList = List[String]()
    hosts.foreach(host => {
       rList = rList ++ exec(cmd, host)
      Thread.sleep(pauseSeconds * 1000)
    })
    rList
  }

  def exec(cmds: List[String], hosts: List[String]): List[String] =  {
    exec(cmds, hosts, 0)
  }

  def exec(cmds: List[String], hosts: List[String], pauseSeconds: Integer): List[String] =  {
    var rList = List[String]()
    hosts.foreach(host => {
      cmds.foreach(cmd => {
        rList = rList ++ exec(cmd, host)
        Thread.sleep(pauseSeconds * 1000)
      })
    })
    rList
  }

  def execJava(cmds: java.util.List[String], hosts: java.util.List[String], pauseSeconds: Integer, widthFirst: Boolean): java.util.List[String] = {
    if(widthFirst){
    	execWidthFirst(cmds.asScala.toList, hosts.asScala.toList, pauseSeconds)
    }else{
    	exec(cmds.asScala.toList, hosts.asScala.toList, pauseSeconds)
    }
  }

  def execWidthFirst(cmds: List[String], hosts: List[String]) {
    execWidthFirst(cmds, hosts, 0)
  } 

  def execWidthFirst(cmds: List[String], hosts: List[String], pauseSeconds: Integer): List[String] =  {
    var rList = List[String]()
    cmds.foreach(cmd => {
      hosts.foreach(host => {
         rList = rList ++ exec(cmd, host)
        Thread.sleep(pauseSeconds * 1000)
      })
    })
    rList
  }
  
  def getMachineNames(): Collection[String] = {
       val t = new RemoteFunctionTask[String](MachineInfo.getMachineName())
       ExtendInvocationService.queryOnAllMembers(t)
  }

  def threadDumpForJvmGroup(jvmGroup: String) {
    val keys = LaunchableCache.getLaunchableKeysForNodeGroup(jvmGroup)
    if (keys.size == 0)
      LOG.info("No nodes associated with nodegroup " + jvmGroup)
    else
      threadDump(keys)
  }

  def threadDumpForJvmConfig(jvmConfig: String): List[ThreadDump] = {
    val keys = LaunchableCache.getLaunchableKeysForJvmConfig(jvmConfig)
    if (keys.size == 0) {
      LOG.info("No nodes associated with nodegroup " + jvmConfig)
      Nil
    } else {
      threadDump(keys)
    }
  }

  def getDumpForJvmConfig(jvmConfig: String) {
    val keys = LaunchableCache.getLaunchableKeysForJvmConfig(jvmConfig)
    if (keys.size == 0)
      LOG.info("No nodes associated with nodegroup " + jvmConfig)
    else
      threadDump(keys)
  }

  def getLogsForJvmConfig(jvmConfig: String): String = {
    getLogs(LaunchableCache.getLaunchableKeysForJvmConfig(jvmConfig))
  }

  def getLogs(launchableKeys: java.util.Collection[LaunchableKey]): String = {
    getLogs(launchableKeys.asScala)
  }

  //  def getLogs(launchableKeys: Collection[LaunchableKey]): String = {
  //    var result = ""
  //    launchableKeys.foreach(a => result += getLogs(a))
  //    result
  //  }

  def getLogs(launchableKey: LaunchableKey): String = {
    LoggingCache.getLog(launchableKey)
  }

  def getLaunchables() = {
    LaunchableCache.values()
  }

  def getLaunchable(key: LaunchableKey) = {
    LaunchableCache.get(key)
  }

  def getConfig(): String = {
    ConfigCache.getConfig()
  }

  def getConfigName(): String = {
    val xml = XML.loadString(ConfigCache.getConfig())
    val clusterconfig = scalaxb.fromXML[org.gridvise.xmlbindings.ClusterConfig](xml)
    clusterconfig.name
  }

  def saveConfig(config: String) = {
    stop()
    ConfigCache.putConfig(config)
    LOG.info("config now is " + ConfigCache.getConfig)
    LaunchableCache.clear()
    LOG.info("stopped cluster")
    val initTask = new RemoteInitializationTask()
    val r = ExtendInvocationService.queryOnAllMembers(initTask)
    LOG.info("completed reinitializing " + r)
  }

  def getLaunchableKeys() = {
    LaunchableCache.keySet(null);
  }

  def reEvaluateLaunchables() = {
    val task = new RemoteReEvalTask()
    ExtendInvocationService.queryOnAllMembers(task).toList.foreach(s => LOG.info(s))
  }

  def machineNames(): List[String] = {
    val task = new RemoteFunctionTask[String](MachineInfo.getMachineName())
    ExtendInvocationService.queryOnAllMembers(task).toList
  }

  def createDefaultExecutionContext(): ExecutionContext = {
    ExecutionContextFactory(OSOperations.getUserName(), machineNames())
  }

  def createDefaultExecutionContext(machineNames: List[String]): ExecutionContext = {
    ExecutionContextFactory(OSOperations.getUserName(), machineNames)
  }

  def createDefaultExecutionContext(machineNames: java.util.List[String]): ExecutionContext = {
    ExecutionContextFactory(OSOperations.getUserName(), machineNames.toList)
  }

}