package org.gridvise.event
import org.gridvise.logical.Launchable
import org.gridvise.logical.ThreadDump

class OSEvent(msg: String, launchable: Launchable, threadDump: ThreadDump) extends Event(launchable, threadDump){

  override def toString() = "[OSEvent: msg=" + msg + "]\n"+this.threadDump

}