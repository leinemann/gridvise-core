package org.gridvise.event.streams.oschecks

import org.gridvise.event.os.MissingPIDEvent
import org.gridvise.event.streams.Check
import org.gridvise.logical.os.OSOperations
import org.gridvise.logical.Launchable
import org.gridvise.mgmtcache.coh.entity.events.EventCache
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableCache
import org.slf4j.LoggerFactory

class PIDsStillExistChecker extends Check {

  def logger = LoggerFactory.getLogger(this.getClass)
  var passed = false
  var verboseResult = "all good"

  override def perform() = {
    val l = LaunchableCache.getLaunchablesOnThisMachine()
    var pidListOK = List[String]()
    var pidListMissing = List[String]()
    l.foreach(a =>
      if (a.isRunning() && !OSOperations.isValidPid(a.processIdentifier)) {
        fireMissingProcessEvent(a)
        a.notRunning()
        a.processIdentifier :: pidListMissing
      } else {
        a.processIdentifier :: pidListOK
      })
    passed = (pidListMissing.size == 0)
    if (!passed)
      verboseResult = "missing pids " + pidListMissing
    else
      verboseResult = "all expected pids found"

    this.createCheckResult(verboseResult, passed)
  }

  def fireMissingProcessEvent(launchable: Launchable) {

    logger.info("PID is missing:%s".format(launchable.processIdentifier))
    EventCache.store(new MissingPIDEvent(launchable))
  }

}