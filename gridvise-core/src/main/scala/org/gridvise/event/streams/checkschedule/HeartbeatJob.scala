package org.gridvise.event.streams.checkschedule

import org.quartz.Job
import org.quartz.JobExecutionContext
import org.gridvise.mgmtcache.coh.entity.heartbeat.HeartbeatCache

class HeartbeatJob extends Job{
	def execute(jobExecContext: JobExecutionContext)  {
	  val name = jobExecContext.getJobDetail().getKey().getName()
	  HeartbeatCache.fireHeartbeat(name)
	}
}