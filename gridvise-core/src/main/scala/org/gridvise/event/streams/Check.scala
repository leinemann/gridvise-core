package org.gridvise.event.streams
import java.util.Date

abstract class Check {
  
  def perform(): CheckResult

  protected def createCheckResult(verboseResult: String, passed: Boolean) = {
    new CheckResult(getId(), verboseResult, passed)
  }

  private def getId() = new CheckId(this.getClass().asInstanceOf[Class[Check]], new Date())

}


