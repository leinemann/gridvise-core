package org.gridvise.event.streams.jmx
import org.gridvise.event.streams.Check
import org.gridvise.event.streams.CheckResult
import org.gridvise.util.jmx.ValueAddress
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableCache
import org.gridvise.util.jmx.LocalMBeanServerConnections
import org.gridvise.logical.Launchable
import org.gridvise.event.JmxEvent
import org.gridvise.mgmtcache.coh.entity.events.EventCache

class JmxCheck(address: String, name: String, threashold: Integer, comparisonType: String) extends Check {

  val valueAddress = new ValueAddress(address, name)

  val LESS_THEN = "LessThen"
  val GREATER_THEN = "GreaterThen"
  val EQUALS = "Equals"
    
  val COH_NOT_INIT = -1

  def getComparison(): (Integer, Integer) => Boolean = {
    if (comparisonType.equals(LESS_THEN))
      (x, y) => {
        x < y
      }
    else if (comparisonType.equals(GREATER_THEN))
      (x, y) => x > y
    else if (comparisonType.equals(EQUALS))
      (x, y) => x == y
    else
      throw new Exception("unimplemented comparison type " + comparisonType)
  }

  def perform(): CheckResult = {
    var passed = true
    var violationCount = 0
    LaunchableCache.getLaunchablesOnThisMachine().foreach(l => {
      if (l.isRunning()) {
        var values = LocalMBeanServerConnections.getValues(valueAddress, l.processIdentifier)
          .filter(e => e._2 != COH_NOT_INIT)
          .filter(e => getComparison()(e._2, threashold))
        if (values.size > 0) {
          fireJMXViolationEvent(values, l)
          passed = false
          violationCount += values.size
        }
      }

    })
    val msg = if (passed) "all good" else violationCount + " jmx values out of range"
    this.createCheckResult(msg, passed)
  }

  def fireJMXViolationEvent(jmxValues: Map[String, Int], l: Launchable) {
    val td = l.threadDump()
    jmxValues.foreach(e => {
      val msg = "found " + e._1 + " to be " + e._2 + ".  It was not expected to be " + this.comparisonType + " " + threashold + "."
      EventCache.store(new JmxEvent(msg, this.address, l, td))
    })

  }

}