package org.gridvise.event.streams.oschecks
import java.util.Date
import org.gridvise.event.os.HighCPUEvent
import org.gridvise.event.os.HighCPULaunchableEvent
import org.gridvise.event.streams.Check
import org.gridvise.event.streams.CheckResult
import org.gridvise.logical.os.OSOperations
import org.gridvise.logical.os.ProcInfo
import org.gridvise.logical.ThreadDump
import org.gridvise.mgmtcache.coh.entity.config.ConfigCache
import org.gridvise.mgmtcache.coh.entity.events.EventCache
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableCache
import org.gridvise.mgmtcache.coh.ManagementCacheServer

class CPPUChecker extends Check {

  def perform(): CheckResult = {
    val cpuInfo = OSOperations.getCPUUsageHigherThen(ConfigCache.getCPUCheckThreashold())
    if (cpuInfo.size == 0) {
      this.createCheckResult("all good", true)
    } else {

      val launchables = LaunchableCache.getLaunchablesOnThisMachine()

      val pidListlaunchable = launchables.map(l => l.processIdentifier)
      val pidListCPU = cpuInfo.map(p => p.id)

      val highLaunchables = launchables.filter(l => pidListCPU.contains(l.processIdentifier))

      val dumpDate = new Date()
      val launchableDumpsByPID = highLaunchables.map(l => (l.processIdentifier, l.threadDump(dumpDate))).toMap

      //TODO thread dump of mgmt server

      val procLaun = cpuInfo.filter(p => (pidListlaunchable.contains(p.id)))
      val procMgmt = cpuInfo.filter(p => (p.cmd.indexOf(ManagementCacheServer.getClass().getName()) != -1))
      val procOther = cpuInfo.filterNot(p => procLaun.contains(p) || procMgmt.contains(p))

      fireLaunchableCPUEvent(procLaun, launchableDumpsByPID)
      fireMgmtCPUEvent(procMgmt)
      fireOtherCPUEvent(procOther)

      this.createCheckResult("cpu threashold exceeded", false)
    }
  }

  def fireLaunchableCPUEvent(procLaun: List[ProcInfo], dumps: Map[String, ThreadDump]) {
    procLaun.foreach(p => createHighCPUEvent(p, dumps.get(p.id).get))
  }

  def createHighCPUEvent(p: ProcInfo, dump: ThreadDump) = {
    var l = LaunchableCache.getLaunchableForPid(p.id)
    EventCache.store(new HighCPULaunchableEvent(p, l, dump))
  }

  def fireMgmtCPUEvent(procLaun: List[ProcInfo]) {
    procLaun.foreach(p => EventCache.store(new HighCPUEvent(p, "ups, mgmt server taking high cpu")))
  }

  def fireOtherCPUEvent(procLaun: List[ProcInfo]) {
    procLaun.foreach(p => EventCache.store(new HighCPUEvent(p, "The following command is taking " + p.cpu + "% CPU: " + p.cmd)))
  }

}