package org.gridvise.event.streams
import java.util.Date

class CheckId(checkClass: Class[Check], date: Date) extends Serializable{
  override def toString() = checkClass.getSimpleName()+"@"+date
}

