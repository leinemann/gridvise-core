package org.gridvise.event.streams.checkschedule

import scala.collection.JavaConversions._
import org.gridvise.event.streams.Check

class ScheduleConfig(var scheduleName: String, var interval: Integer) {

  //FIXME this variable declaration and constructor are on here because of Spring
  var checks: List[Check] = _

  def this(scheduleName: String, interval: Integer, checks: java.util.List[Check]) = {
    this(scheduleName, interval)
    this.checks = checks.toList

  }
}
