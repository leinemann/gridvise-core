package org.gridvise.event.streams.checkschedule
import org.quartz.Job
import scala.collection.JavaConversions._
import org.gridvise.mgmtcache.coh.entity.heartbeat.HeartbeatCache
import org.gridvise.util.Scheduler

class CheckScheduler {

  val heartbeatJobClass = new HeartbeatJob().getClass().asInstanceOf[Class[Job]]

  //FIXME ugly conversion because of spring
  def setScheduleConfig(scheduleConfigsList: java.util.List[ScheduleConfig]) {
    try {
      val scheduleConfigs = scheduleConfigsList.toList
      scheduleConfigs.foreach(sc => {
        Scheduler.schedule(heartbeatJobClass, sc.scheduleName, sc.interval)
        sc.checks.foreach(
          check => {
            HeartbeatCache.addHeartbeatListener(new Listener(check), sc.scheduleName)
          })
      })
    } catch {
      case e: Throwable => {
        e.printStackTrace()
      }
    }
  }

}