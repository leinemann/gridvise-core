package org.gridvise.event.streams.checkschedule
import org.gridvise.event.streams.Check
import org.gridvise.mgmtcache.coh.entity.checkresult.CheckResultCache
import org.gridvise.mgmtcache.coh.entity.heartbeat.HeartbeatCache

class Listener(check: Check) extends HeartbeatCache.HeartbeatListener {
  def tick() {
    var checkResult = check.perform()
    CheckResultCache.store(checkResult)
  }
}