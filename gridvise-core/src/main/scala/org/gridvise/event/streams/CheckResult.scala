package org.gridvise.event.streams

class CheckResult(var id: CheckId, verbouseResult: String, passed: Boolean) extends Serializable {
  override def toString() = id + " -> " + verbouseResult + ": passed="+passed
}