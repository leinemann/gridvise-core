package org.gridvise.event
import org.gridvise.logical.ThreadDump
import org.gridvise.logical.Launchable

class JmxEvent(msg: String, address: String, launchable: Launchable, threadDump: ThreadDump) extends Event(launchable, threadDump) {
  override def toString() = "[JmxEvent: msg=" + msg + "]+\n"+this.threadDump
}