package org.gridvise.event
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey
import org.gridvise.xmlbindings.DictionaryEntryable
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableCache

class LogEvent(launchableKey: LaunchableKey, dictionaryEntry: DictionaryEntryable, value: String) extends Event(LaunchableCache.get(launchableKey), LaunchableCache.get(launchableKey).threadDump()){
  override def toString() = "[LogEvent: launchableKey = " + launchableKey + ", dictionaryEntry=" + dictionaryEntry + ", value=" + value + "]"
}