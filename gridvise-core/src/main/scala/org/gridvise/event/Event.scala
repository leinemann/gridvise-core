package org.gridvise.event
import org.gridvise.logical.Launchable
import org.gridvise.logical.ThreadDump
import org.gridvise.mgmtcache.coh.entity.events.EventKey
import org.gridvise.logical.gridsequence.LongGridSequence
import org.gridvise.logical.os.MachineInfo

class Event(val launchalbe: Launchable, val threadDump: ThreadDump) extends Serializable {

  val NOT_APPLICABLE = "n/a"

  def eventKey() = {
    if (this.launchalbe != null) {
      eventKeyBasedOnLaunchable()
    } else {
      defaultEventKey()
    }
  }

  def eventKeyBasedOnLaunchable() = {
    new EventKey(LongGridSequence.nextId(), getClass().getSimpleName(), launchalbe.machineName, launchalbe.nodeGroupName, launchalbe.configName)
  }

  def defaultEventKey() = {
    new EventKey(LongGridSequence.nextId(), getClass().getSimpleName(), MachineInfo.getMachineName(), NOT_APPLICABLE, NOT_APPLICABLE)
  }

}