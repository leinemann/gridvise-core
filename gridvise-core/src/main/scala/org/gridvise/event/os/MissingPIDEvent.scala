package org.gridvise.event.os
import org.gridvise.event.OSEvent
import org.gridvise.logical.Launchable

class MissingPIDEvent(launchable: Launchable) extends OSEvent("mising PID:"+launchable.processIdentifier, launchable, null)
