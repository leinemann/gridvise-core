package org.gridvise.event.os
import org.gridvise.event.OSEvent
import org.gridvise.logical.ThreadDump
import org.gridvise.logical.os.ProcInfo
import org.gridvise.logical.Launchable

class HighCPULaunchableEvent(procInfo: ProcInfo, launchable: Launchable, dump: ThreadDump)
  extends OSEvent("[HighCPULaunchableEvent: procInfo = " + procInfo + "]", launchable, dump)