package org.gridvise.event.os
import org.gridvise.event.OSEvent
import org.gridvise.logical.os.ProcInfo

class HighCPUEvent(procInfo: ProcInfo, msg: String)
	extends OSEvent("[HighCPUEvent: procInfo = " + procInfo.id + ", msg=" + msg + "]", null, null)