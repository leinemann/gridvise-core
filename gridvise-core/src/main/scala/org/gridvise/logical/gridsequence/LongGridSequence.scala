package org.gridvise.logical.gridsequence
import org.gridvise.mgmtcache.coh.entity.gridsequence.GridSequenceCache

object LongGridSequence {

  var allocatedRange = GridSequenceCache.nexRange()
  var currentId = allocatedRange.start

  def nextId() = {
    //TODO atomic ints
    GridSequenceCache
    synchronized{
      currentId = currentId + 1
      if (currentId > allocatedRange.end) {
        allocatedRange = GridSequenceCache.nexRange()
        currentId = allocatedRange.start
      }
      currentId
    }
  }

}