package org.gridvise.logical

import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.xml.XML
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableCache
import org.gridvise.xmlbindings._
import org.gridvise.logical.os.MachineInfo
import org.slf4j.LoggerFactory
import org.gridvise.logical.gridsequence.LongGridSequence
import org.gridvise.xmlbindings.JvmConfig
import org.gridvise.xmlbindings.Server
import org.gridvise.xmlbindings.ClusterConfig
import org.gridvise.xmlbindings.JvmGroup
import scala.collection.JavaConverters._

object ClusterConfigParser {

  val LOG = LoggerFactory.getLogger(ClusterConfigParser.getClass())
  val hostnameVarName = "HOSTNAME"
  val uniqueNameVarName = "uniqueName"

  val hostname = MachineInfo.getMachineName()

  var clusterName = ""
  var clusterMode = ""

  var clusterconfig: ClusterConfig = _
  var server: Server = _

  val jvmConfigs = new HashMap[String, JvmConfig]
  val cmdConfigs = new HashMap[String, CmdConfig]
  val jvmGroups = new HashMap[String, JvmGroup]
  val sysVars = new HashMap[String, String]
  val overrideVars = new HashMap[String, String]

  def initialize(xmlIncoming: String) = {
    LOG.info("Initializing on hostname=%s".format(hostname))
    val xmlString = replaceSysVarRefs(xmlIncoming)
    val xml = XML.loadString(xmlString)
    clusterconfig = scalaxb.fromXML[org.gridvise.xmlbindings.ClusterConfig](xml)
    clusterName = clusterconfig.name
    clusterMode = clusterconfig.mode
    assignServer()
    indexJvmConfigs()
    indexCmdConfigs()
    indexJvmGroups()
    setOverideVariables()
    indexSystemVariables()
    insertLaunchables()
  }


  private def getLaunchables(): HashSet[Launchable] = {
    var launchables = new HashSet[Launchable]()
    if (server != null) {
      for (n <- ClusterConfigParser.this.server.NodeGroupRef) yield {
        val g = ClusterConfigParser.this.jvmGroups.get(n.name).get
        for (j <- g.Jvm) yield {
          var c = 0
          times(j.count) ({
            c = c+1
           // logger.info("%s".format(jvmConfigs.get(j.configRef).get))

            var l :Launchable = null

            var jvmConfig = jvmConfigs.get(j.configRef)
            var cmdConfig = cmdConfigs.get(j.configRef)
            if(!jvmConfig.isEmpty){
              l = ClusterConfigParser.this.createJavaLaunchable(jvmConfig.get, n.name.toString(), c)
            }else if(!cmdConfig.isEmpty){
              l = ClusterConfigParser.this.createCmdLaunchable(cmdConfig.get, n.name.toString(), c)
            }else{
              throw new RuntimeException("no config retrieved for "+j.configRef)
            }

            l.displayName = l.configName + " " + c  + " (stopped)"
            if (!j.delay.isEmpty) {
              l.delay = j.delay.get
            }
            launchables.add(l)
          })
        }
      }
    } else {
      LOG.info("No launchables configured for this host")
    }
    launchables
  }

  private def insertLaunchables() {
    getLaunchables().foreach(l => LaunchableCache.putLaunchable(l))
  }

  private def indexJvmConfigs() {
    for (c <- clusterconfig.JvmConfigs.JvmConfig) yield {
      ClusterConfigParser.this.sysVars.put(ClusterConfigParser.this.uniqueNameVarName, c.uniqueName)
      ClusterConfigParser.this.jvmConfigs.put(c.uniqueName, c)
    }
  }

  private def indexCmdConfigs() {
    for (c <- clusterconfig.CmdConfigs.CmdConfig) yield {
      ClusterConfigParser.this.sysVars.put(ClusterConfigParser.this.uniqueNameVarName, c.uniqueName)
      ClusterConfigParser.this.cmdConfigs.put(c.uniqueName, c)
    }
  }

  private def indexSystemVariables() {

    val p = System.getProperties().asScala.toMap ++ System.getenv().asScala.toMap

    LOG.info("using system variables "+p)

    for (c <- clusterconfig.RequiredSystemVars.SystemVar) yield {
      val v = getOverrideOption(c.name)
      if (v != null) {
        ClusterConfigParser.this.sysVars.put(c.name, v)
      } else {
        var v = p.get(c.name)
        if (v.isEmpty) {
          LOG.warn("WARNING: %s not provided as environment variable.".format(c.name))
          if (c.description != null) {
            LOG.info("%s is expected as: %s".format(c.name, c.description))
          }
        }else{
          ClusterConfigParser.this.sysVars.put(c.name, v.get)
        }
      }
    }
    ClusterConfigParser.this.sysVars.put(ClusterConfigParser.this.hostnameVarName, hostname)
  }

  private def getOverrideOption(name: String) = {
    val overrideOption = ClusterConfigParser.this.overrideVars.get(name)
    if (!overrideOption.isEmpty) {
      overrideOption.get
    } else {
      null
    }
  }

  private def setOverideVariables() {
    for (c <- clusterconfig.OverrideSystemVars.OverrideVar) yield {
      ClusterConfigParser.this.overrideVars.put(c.name, c.dir)
    }
  }
  private def indexJvmGroups() {
    for (c <- clusterconfig.JvmGroups.JvmGroup) yield {
      ClusterConfigParser.this.jvmGroups.put(c.name, c)
    }
  }

  private def assignServer() = {
    for (s <- clusterconfig.Servers.Server) yield {
      if (ClusterConfigParser.this.hostname.startsWith(s.hostname)) {
        ClusterConfigParser.this.server = s
      }
    }
  }

  private def createJavaLaunchable(jvmConfig: JvmConfig, nodeGroupName: String, ordinal: Integer): Launchable = {
    var l = new JavaLaunchable(LongGridSequence.nextId(), ordinal)
    setProperties(jvmConfig, l)
    l.nodeGroupName = nodeGroupName
    LOG.info("Created launchable %s".format(l))
    l
  }

  private def createCmdLaunchable(cmdConfig: CmdConfig, nodeGroupName: String, ordinal: Integer): Launchable = {
    var l = new CmdLineLaunchable(LongGridSequence.nextId(), cmdConfig, ordinal)
    l.nodeGroupName = nodeGroupName
    LOG.info("Created launchable %s".format(l))
    l
  }

  private def setProperties(jvmConfig: JvmConfig, l: Launchable): Unit = {

    if (!jvmConfig.parent.isEmpty) {
      val parentJvmConfigName = jvmConfig.parent.get
      if (parentJvmConfigName != null) {
        var parentJvmConfig = ClusterConfigParser.this.jvmConfigs.get(parentJvmConfigName)
        if (parentJvmConfig != null) {
          setProperties(parentJvmConfig.get, l)
        } else {
          LOG.warn("WARNING: reference to parent %s not found".format(jvmConfig.parent.get))
        }
      }
    }

    l.configName = jvmConfig.uniqueName

    if (!jvmConfig.mainClass.isEmpty) {
      l.mainClass = jvmConfig.mainClass.get
    }
    if (!jvmConfig.jvmArgs.isEmpty) {
      l.jvmArgs = jvmConfig.jvmArgs.get
    }
    for (x <- jvmConfig.JavaOptions.Option)
      l.jvmOptions.put(x.name,x.valueAttribute)

    for (x <- jvmConfig.Classpath)
      for (d <- x.Dir)
        l.classPathEntries.add(sysVars.get(d.systemVar.get).get + "/" + d.relativePath.get)
  }

  private def replaceSysVarRefs(xmlString: String): String = {
    val xml = XML.loadString(xmlString)
    clusterconfig = scalaxb.fromXML[org.gridvise.xmlbindings.ClusterConfig](xml)
    setOverideVariables()
    indexSystemVariables()

    var returnString = xmlString
    ClusterConfigParser.this.sysVars foreach { case (varName, varValue) => {
        println(varName)
        println(varValue)
        returnString = returnString.replaceAll("\\$\\{" + varName + "\\}", varValue)
      }
    }
    returnString
  }

  private def times(n: Int)(code: => Unit) = {
    for (i <- 1 to n) code
  }

}