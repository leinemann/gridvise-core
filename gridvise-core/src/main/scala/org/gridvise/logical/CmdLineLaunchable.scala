package org.gridvise.logical

import java.lang.String
import org.gridvise.logical.os.MachineInfo
import org.gridvise.logical.os.OSOperations
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey
import org.gridvise.xmlbindings.CmdConfig
import org.slf4j.LoggerFactory
import sys.process.stringSeqToProcess
import scala.sys.process._
import org.gridvise.logical.logbroker.LogBroker
import org.gridvise.logical.os.OSOperations
import java.io.FileInputStream
import org.gridvise.util.AsyncInvoker

class CmdLineLaunchable(clusterId: Long, cmdConfig: CmdConfig, ordinal: Integer) extends Launchable(new LaunchableKey(MachineInfo.getMachineName(), clusterId), ordinal) {

  val LOG = LoggerFactory.getLogger(getClass())

  
  configName = cmdConfig.uniqueName

  override def buildCommand(): String = {
    addOrdinal(cmdConfig.base+"/"+cmdConfig.startCmd)
  }

  override def stop() {
	 val cmd = cmdConfig.base+"/"+addOrdinal(cmdConfig.stopCmd)
     val r = OSOperations.exec(cmd)
     notRunning()
     LOG.info(r)
  }

  def addOrdinal(cmd: String): String = {
    cmd.replaceAll("\\$\\{COUNTER\\}", ordinal+"")
  }

  def getProcessIdentifier(p: scala.sys.process.Process): String = {
    var pidFile =  cmdConfig.pidFile
    pidFile = pidFile.replaceAll("\\$\\{COUNTER\\}", ordinal+"")
    var i = 0
    var pid = "not found"
    while(!OSOperations.isValidPid(pid)){
      i += 1
      if(i > 10){
        return pid
      }
      try{
        LOG.info("attempting to read pid from file "+pidFile)
        pid = io.Source.fromInputStream(getClass.getClassLoader.getResourceAsStream(pidFile)).mkString.trim()
        LOG.info("pid = '"+pid+"'")
      }catch{
        case e: Exception => {
          LOG.info("couldn't find "+ pidFile)
        }
      }
      Thread.sleep(1000)
    }
    LOG.info("pid from file is pid "+pid)
    val childPid = OSOperations.getChildPid(pid)
    LOG.info("childPid is pid '"+childPid+"'")
    childPid
  }
  
  def registerLogFile() = {
    var l = cmdConfig.logFile
    l = l.replaceAll("\\$\\{HOST\\}", MachineInfo.getMachineName+"")
    l = l.replaceAll("\\$\\{COUNTER\\}", ordinal+"")
    l = l.replaceAll("\\$\\{PID\\}", processIdentifier+"")
    LOG.info("attempting to read log file '"+l+"'")
    AsyncInvoker.async {
    		scala.io.Source.fromInputStream(getClass.getClassLoader.getResourceAsStream(l)).getLines().foreach(l => LogBroker.handleInfoLine(getLaunchableKey(), l))
    	}
  }

override def toString() : String = {
    super.toString() + this.cmdConfig.startCmd
  }


}
