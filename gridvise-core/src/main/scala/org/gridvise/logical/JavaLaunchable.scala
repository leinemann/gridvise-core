package org.gridvise.logical

import java.lang.String
import org.gridvise.logical.os.{MachineInfo, OSOperations}
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey
import scala.sys.process._


/**
 * Created with IntelliJ IDEA.
 * User: christoph
 * Date: 16/11/2013
 * Time: 11:06
 * To change this template use File | Settings | File Templates.
 */
class JavaLaunchable(launchableKey: LaunchableKey, ordinal: Integer) extends Launchable(launchableKey, ordinal){

  def this(clusterId: Long, ordinal: Integer) = this(new LaunchableKey(MachineInfo.getMachineName(), clusterId), ordinal)


  override def buildCommand(): String = {
    //java -classpath C:\java\MyClasses;C:\java\OtherClasses ...
    var command = OSOperations.getJavaCommand()

    command += appendJVMOptions(command)
    command += appendClasspathEntries(command)

    command += " " + mainClass
    if ("null" != jvmArgs + "") command += " " + jvmArgs

    command
  }


  def appendClasspathEntries(command: String): String = {
    //java -classpath C:\java\MyClasses;C:\java\OtherClasses ...
    var cpFragment = " -classpath "
    classPathEntries.foreach { cpe =>
      cpFragment += (cpe + OSOperations.getClasspathSeparator())
    }
    removeLastCharacter(cpFragment)
  }

  def appendJVMOptions(command: String): String = {
    var optionFragment = ""
    jvmOptions.foreach {
      case (key, value) => {
        var v = value.replaceAll(COUNTER_VAR_PACEHOLDER, this.ordinal.toString())
        v = v.replaceAll(JVM_NAME_VAR_PACEHOLDER, this.configName)
        optionFragment += " " + key + v
      }
    }
    optionFragment
  }

  def removeLastCharacter(in: String): String = {
    in.substring(0, in.length() - 1)
  }

  def getProcessIdentifier(p: scala.sys.process.Process): String = {
    OSOperations.getProcessIdentifier(p)
  }
  
  def registerLogFile() = {
    //TODO
  }
}
