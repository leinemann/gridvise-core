package org.gridvise.logical.runbook.activity

import java.util.Date

abstract class ActivityContext(val runBookId: String, vars: Map[String, String]) extends Serializable {

  def perform():String
  def id:String

  var startTime = new Date()
  var endTime = new Date()

  def replaceVars(cmd: String):String = {
    var r = cmd
    vars.foreach(x => r = r.replaceAllLiterally("${"+x._1+"}", x._2))
    r
  }
}
