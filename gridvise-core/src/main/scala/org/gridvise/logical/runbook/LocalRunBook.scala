package org.gridvise.logical.runbook

import org.gridvise.mgmtcache.coh.entity.runbook.RunBookCache
import org.gridvise.logical.runbook.activity.{ActivityContextFactory, ActivityContext}
import org.gridvise.mgmtcache.coh.entity.activityresult.ActivityResultCache
import scala.xml.{Elem, XML}
import org.slf4j.LoggerFactory
import org.gridvise.xmlbindings.RunBook
import org.gridvise.logical.os.MachineInfo


object LocalRunBook {

  val logger = LoggerFactory.getLogger(getClass)

  def runActivity(runBookId: String, activityId: String, vars: Map[String, String]): String = {
    val runBook = RunBookCache.get(runBookId)
    val a = runBook.Activities.Activity.find(x => x.id.equals(activityId))
    perform(ActivityContextFactory.createActivityContext(runBookId, a.get, vars))
  }

  def runBook(runBookId: String, vars: Map[String, String]): String = {
    val runBook = RunBookCache.get(runBookId)
    var result = ""
    runBook.Activities.Activity.foreach( a => result = result + a.id+"=>"+perform(ActivityContextFactory.createActivityContext(runBookId, a, vars))+";")
    result
  }

  def perform(context: ActivityContext): String = {
    val result = context.perform()
    ActivityResultCache.putActivity(context, result)
    result
  }

  def readFromClasspath(fileClasspathLocation: String): Elem = {

    try{
      println("loading runbook from "+fileClasspathLocation)
      XML.load(getClass().getClassLoader().getResourceAsStream(fileClasspathLocation))
    }catch {
      case e : Exception => {
        println("failed from classloader trying again from class "+fileClasspathLocation)
        XML.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(fileClasspathLocation))
      }
    }
  }

  def loadRunBook(fileClasspathLocation : String): String = {
      logger.info("loading RunBook from "+fileClasspathLocation)
      var runBookXML = readFromClasspath(fileClasspathLocation)
      val runBook = scalaxb.fromXML[RunBook](runBookXML)
      RunBookCache.putRunBook(runBook)
      val msg = "completed loading RunBook "+runBook.id +" on "+MachineInfo.getMachineName()
      logger.info(msg)
      runBook.id
  }


}
