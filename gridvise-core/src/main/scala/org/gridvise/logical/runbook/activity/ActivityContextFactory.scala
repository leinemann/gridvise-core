package org.gridvise.logical.runbook.activity

import org.gridvise.xmlbindings.{COMMAND_LINE, SCALA, Activity}

object ActivityContextFactory {

  def createActivityContext(runBookId: String, activity: Activity, vars: Map[String, String]) = {

    //TODO do via case class ?
      if(activity.activityType == SCALA){
          new ScalaActivityContext(runBookId, activity, vars)
      }else if(activity.activityType == COMMAND_LINE){
        new CommandLineActivityContext(runBookId, activity, vars)
      }else{
        throw new Exception("activity type "+ activity.activityType +" not implemented")
      }


  }
}
