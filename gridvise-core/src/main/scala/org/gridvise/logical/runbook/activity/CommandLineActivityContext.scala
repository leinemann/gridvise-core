package org.gridvise.logical.runbook.activity

import scala.sys.process._
import org.slf4j.LoggerFactory
import java.util.Date
import org.gridvise.xmlbindings.Activity

class CommandLineActivityContext(runBookId: String, commandLine: Activity, vars: Map[String, String]) extends ActivityContext(runBookId, vars){

  val logger = LoggerFactory.getLogger(getClass)

  def perform(): String = {
    startTime = new Date()
    logger.info("performing runbook activiy id="+commandLine.id)
    val cmd = replaceVars(commandLine.value)
    logger.info(cmd)
    val result = cmd.trim.!!.trim
    logger.info("command completed with result '"+result+"'")
    endTime = new Date()
    result
  }

  def id = commandLine.id

}
