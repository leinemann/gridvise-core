package org.gridvise.logical
import scala.collection.mutable.HashMap

class ThreadDump(dump: String, machineName: String, nodeGroupName: String, configName: String) extends Serializable {

  val START_OF_FIRST_LINE = "\""
  val THREAD_STATE_RUNNABLE = "RUNNABLE"

  override def toString() = "[" + machineName + "/" + nodeGroupName + "/" + configName + "]\n" + dump + "\n\n"

  def filterRunnable() = {
    this.splitByThread().filter(
      e => e._2.contains(THREAD_STATE_RUNNABLE)).values.toList
  }

  def splitByThread() = {

    var currentThread = ""
    var currentName = ""

    var dumps = HashMap[String, String]()

    this.dump.lines.foreach(s => {
      if (this.isFirstLine(s)) {
        dumps(currentName) = currentThread
        currentThread = s
        currentName = s
      } else {
        currentThread = currentThread + "\n" + s
      }
    })

    dumps
  }

  def getThreadName(firstLine: String) = {
    firstLine.substring(1, firstLine.lastIndexOf("\""))
  }

  def isFirstLine(line: String) = line.startsWith(START_OF_FIRST_LINE);

}