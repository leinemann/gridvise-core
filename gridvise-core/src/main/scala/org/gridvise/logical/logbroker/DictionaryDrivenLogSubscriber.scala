package org.gridvise.logical.logbroker

import org.gridvise.logical.logbroker.LogBroker.LogSubscriber
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey
import org.gridvise.xmlbindings.DictionaryEntryable

class DictionaryDrivenLogSubscriber(dicEntry: DictionaryEntryable, launchableKey: LaunchableKey) extends LogSubscriber {

  var stdout: String = ""
  var stderr: String = ""

  var isComplete: Boolean = false

  def logLine(line: String) {
    stdout = stdout + "\n" + line
    checkComplete(line)
  }

  def checkComplete(line: String) {
    if (line.indexOf(dicEntry.lineMatch) != -1) {
      setComplete()
      LogBroker.unsubscribe(launchableKey, this)
      synchronized { notify() }
    }
  }

  def logError(line: String) = stderr = stderr + "\n" + line

  def out(): String = {
    stdout + stderr
  }

  def setComplete() {
    isComplete = true
  }

}