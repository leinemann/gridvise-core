package org.gridvise.logical.logbroker
import scala.collection.mutable.Map
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey
import org.gridvise.mgmtcache.coh.entity.logging.LoggingCache
import org.gridvise.xmlbindings.DictionaryEntryable
import org.gridvise.mgmtcache.coh.entity.logging.LoggingKey
import org.slf4j.LoggerFactory

object LogBroker {

  def logger = LoggerFactory.getLogger(this.getClass())
  
  var subscribers: Map[LaunchableKey, List[LogSubscriber]] = Map[LaunchableKey, List[LogSubscriber]]()
  var lineNumbers: Map[LaunchableKey, Long] = Map[LaunchableKey, Long]()

  abstract class LogSubscriber {
    var isComplete: Boolean
    def logLine(line: String)
    def logError(line: String)
    def out: String
  }

  def handleInfoLine(launchableKey: LaunchableKey, line: String) {
    val lk = new LoggingKey(this.getLineNumberForLaunchable(launchableKey), launchableKey)
    logger.info("Logging cache:%s -> %s".format(lk,line))
    LoggingCache.put(lk, line)
    handleLogEvent(launchableKey, line, (ls: LogSubscriber, s: String) => ls.logLine(s))
  }

  def handleErrorLine(launchableKey: LaunchableKey, line: String) {
    logger.info("Error line from %s -> %s".format(launchableKey,line))
    handleLogEvent(launchableKey, line, (ls: LogSubscriber, s: String) => ls.logError(s))
  }

  def handleLogEvent(launchableKey: LaunchableKey, line: String, f: (LogSubscriber, String) => Unit) {
    if (subscribers.contains(launchableKey))
      subscribers(launchableKey).foreach(a => f(a, line))
  }

  def subscribe(launchableKey: LaunchableKey, entryable: DictionaryEntryable): LogSubscriber = {
    var logSub = new DictionaryDrivenLogSubscriber(entryable, launchableKey);
    addToSubscriptionList(launchableKey, logSub)
    logSub
  }

  def subscribeCallback(launchableKey: LaunchableKey, entryable: DictionaryEntryable, callback: (String) => _): LogSubscriber = {
    var logSub = new CallbackUnsubscribingLogSubscriber(entryable, launchableKey, callback);
    addToSubscriptionList(launchableKey, logSub)
    logSub
  }

  def subscribeEventStream(launchableKey: LaunchableKey, entryable: DictionaryEntryable): LogSubscriber = {
    var logSub = new DictionaryDrivenEventStreamLogSubscriber(entryable, launchableKey);
    addToSubscriptionList(launchableKey, logSub)
    logSub
  }

  def addToSubscriptionList(launchableKey: LaunchableKey, logSub: LogSubscriber) {
    if (!getSubscriptionList(launchableKey).contains(logSub)) {
      subscribers(launchableKey) = logSub :: subscribers(launchableKey)
    }
  }

  def unsubscribe(launchableKey: LaunchableKey, logSubscriber: LogSubscriber) {
    val l = getSubscriptionList(launchableKey).filterNot(ele => ele.equals(logSubscriber))
    if (l.size == 0) {
      subscribers.remove(launchableKey)
    } else {
      subscribers(launchableKey) = l
    }
  }

  def getSubscriptionList(launchableKey: LaunchableKey): List[LogSubscriber] = {
    if (!subscribers.contains(launchableKey)) {
      subscribers(launchableKey) = List[LogSubscriber]()
    }
    subscribers(launchableKey)
  }

  def getLineNumberForLaunchable(launchableKey: LaunchableKey): Long = {
    if (!lineNumbers.contains(launchableKey) || lineNumbers(launchableKey) == Long.MaxValue) {
      lineNumbers(launchableKey) = 0
    } else {
      lineNumbers(launchableKey) = lineNumbers(launchableKey) + 1
    }
    lineNumbers(launchableKey)
  }
}