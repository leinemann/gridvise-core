package org.gridvise.logical.logbroker
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey
import org.gridvise.xmlbindings.DictionaryEntryable
import scala.None

class CallbackUnsubscribingLogSubscriber(dicEntry: DictionaryEntryable, launchableKey: LaunchableKey, callback: (String) => _)
  extends DictionaryDrivenLogSubscriber(dicEntry, launchableKey) {

  var lastLine: String = _

  override def logLine(line: String) {
    lastLine = line
    checkComplete(line)
  }

  override def setComplete() {
    super.setComplete()
    val v = extractValue(lastLine)
    this.callback(v)
  }

  def extractValue(line: String) = {
    //FIXME there must be smarter more concise way of dealing with regex
    val regexp = dicEntry.valueRegexp.get.r
    val m = regexp.findFirstMatchIn(line)
    if (m.size > 0 && m.get.subgroups.size > 0) {
      m.get.subgroups.toList(0)
    } else {
      None.toString()
    }

  }

}