package org.gridvise.logical.logbroker

import org.gridvise.event.LogEvent
import org.gridvise.logical.logbroker.LogBroker.LogSubscriber
import org.gridvise.mgmtcache.coh.entity.events.EventCache
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey
import org.gridvise.xmlbindings.DictionaryEntryable

class DictionaryDrivenEventStreamLogSubscriber(dicEntry: DictionaryEntryable, launchableKey: LaunchableKey) extends LogSubscriber {

  var stdout: String = ""
  var stderr: String = ""

  def logLine(line: String) {
    checkEventCondition(line)
  }

  def checkEventCondition(line: String) {
    if (line.indexOf(dicEntry.lineMatch) != -1) {
      fireEvent(line)
    }
  }

  def fireEvent(line: String) {
    val regexp = dicEntry.valueRegexp.get.r
    val m = regexp.findFirstMatchIn(line)
    var value = ""
    if (m.size > 0 && m.get.subgroups.size > 0) {
      value = m.get.subgroups.toList(0)
    }
    val e = new LogEvent(launchableKey, dicEntry, value)
    //TODO
    EventCache.store(e);
  }

  def logError(line: String) = stderr = stderr + "\n" + line

  //FIXME should out and isComplete really be part of the interface
  def out(): String = ""
  var isComplete: Boolean = false

}