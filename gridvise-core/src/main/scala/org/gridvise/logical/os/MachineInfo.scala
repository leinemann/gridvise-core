package org.gridvise.logical.os
import java.net.InetAddress

object MachineInfo {

  def getMachineName(): String = {
    val name = InetAddress.getLocalHost().getHostName()
    if (name.indexOf(".") == -1)
      name
    else
      name.subSequence(0, name.indexOf(".")).toString()
  }

}