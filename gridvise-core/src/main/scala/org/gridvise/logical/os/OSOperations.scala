package org.gridvise.logical.os

import java.lang.System.getProperty
import org.gridvise.logical.os.support.UnixSupport
import org.gridvise.logical.os.support.WindowsSupport
import org.slf4j.LoggerFactory


object OSOperations extends OSSupport{

  def logger = LoggerFactory.getLogger(OSOperations.getClass())
  
  var osSupportImpl: OSSupport = new UnixSupport()

  initialize()

  def initialize() {
    if (is("win")) {
      osSupportImpl = new WindowsSupport()
      return
    }
    if (is("nix")) {
      osSupportImpl = new UnixSupport()
      return
    }
    if (is("linux")) {
      osSupportImpl = new UnixSupport()
      return
    }
    if (is("mac")) {
      osSupportImpl = new UnixSupport()
      return
    }

     throw new Exception("Operating system not supported by gridvise")
  }

  def is(osNameFragment: String): Boolean = {
    var os = getProperty("os.name").toLowerCase();
    logger.info("os is %s ".format(os))
    os.indexOf(osNameFragment) >= 0
  }

  override def getClasspathSeparator(): String = {
    osSupportImpl.getClasspathSeparator()
  }

  override def getProcessIdentifier(process: scala.sys.process.Process): String = {
    osSupportImpl.getProcessIdentifier(process)
  }

  def stopProcess(processIdentifier: String) = {
    osSupportImpl.stopProcess(processIdentifier)
  }

  def exec(cmd: String): String =  {
    osSupportImpl.exec(cmd)
  }


  def getJavaCommand(): String = {
    osSupportImpl.getJavaCommand()
  }
  
  def setSystemPropery(name: String, value: String) {
    osSupportImpl.setSystemPropery(name, value)
  }

  def threadDump(pid: String) {
    osSupportImpl.threadDump(pid)
  }
  
  override def isValidPid(pid: String) = osSupportImpl.isValidPid(pid)

  override def getChildPid(pid: String) = osSupportImpl.getChildPid(pid)
  
  override def getCPUUsage() = osSupportImpl.getCPUUsage()

  override def getCPUUsageHigherThen(percentage: Int): List[ProcInfo] = osSupportImpl.getCPUUsageHigherThen(percentage)

  override def getUserName(): String = osSupportImpl.getUserName()

  
  
}