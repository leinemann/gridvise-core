package org.gridvise.logical.os.support
import java.lang.management.ManagementFactory
import scala.sys.process._
import org.gridvise.logical.os.OSSupport
import org.gridvise.logical.os.ProcInfo
import org.slf4j.LoggerFactory
import java.util.Arrays

class UnixSupport extends OSSupport {

  val LOG = LoggerFactory.getLogger(this.getClass())
  override def getClasspathSeparator = ":"

  var env = System.getenv()

  override def getJavaCommand(): String = {
    val javaHome = env.get("JAVA_HOME")
    if (javaHome != null)
      javaHome + "/bin/java"
    else
      "java"
  }

  def getProcessIdentifier(process: scala.sys.process.Process): String = {
    // first getting scala.sys.process.ProcessImpl - need to get field p
    val procClass = process.getClass();
    val field = procClass.getDeclaredField("p");
    field.setAccessible(true);
    val p = field.get(process);
    //field p contains java.lang.UNIXProcess which has an int field called pid
    val uproc = p.getClass();
    val pidField = uproc.getDeclaredField("pid");
    pidField.setAccessible(true)
    pidField.get(p).toString()
  }

  def stopProcess(pid: String) {
    ("kill -9 " + pid).run()
  }

  def exec(cmd: String): String = {
    cmd !!
  }

  def threadDump(pid: String) {
    val cmd = "kill -3 " + pid
    LOG.info("invoking " + cmd + " to obtain thread dump")
    cmd.run()
  }

  def getPid(): String = {
    ManagementFactory.getRuntimeMXBean().getName().split("@")(0)
  }

  def setSystemPropery(name: String, value: String) {
    var cmd = "export " + name + "=" + value
    LOG.info("Setting system variable by executing %s".format(cmd))
    cmd.run()
  }

  override def isValidPid(pid: String): Boolean = {
    if (pid == null) return false
    // This uses !! to get the whole result as a string
    // from import scala.sys.process._
    val processes = "ps -e".!!
    //FIXME needs to be sure it's the first number in the line!!
    processes.indexOf(pid) != -1
  }

  override def getChildPid(pid: String): String = {
    val line = "ps xao pid,ppid".!!.split("\n")
      .find(l => {
        pid.equals(l.split(" +")(1))
      })
    LOG.info("pid line is '"+line+"'")
    if (line.isEmpty)
      return pid
    else
      return line.get.split(" +")(0)
  }

  override def getCPUUsage(): List[ProcInfo] = {
    var v = "ps aux".!!
    v.split("\n") filterNot (l => l.startsWith("USER")) map { line =>
      val tokens = line.split(" +")
      new ProcInfo(tokens(0), tokens(1), tokens(2).toDouble, tokens(3).toDouble, tokens(10))
    } toList
  }

  override def getCPUUsageHigherThen(percentage: Int): List[ProcInfo] = {
    if (percentage == 0)
      List[ProcInfo]()
    getCPUUsage().filter(pi => pi.cpu > percentage)
  }

  override def getUserName(): String = "whoami".!!
}