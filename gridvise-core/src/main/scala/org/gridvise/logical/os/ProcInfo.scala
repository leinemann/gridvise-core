package org.gridvise.logical.os

class ProcInfo(val user: String, val id: String, val cpu: Double, val mem: Double, val cmd: String) extends Serializable {

  override def toString() = {
	  "[ProcInfo: user="+ user+";id="+ id+";cpu="+ cpu+";mem="+ mem+";cmd="+ cmd+"]"
  }
}
