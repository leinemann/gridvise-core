package org.gridvise.logical.os.support
import org.gridvise.logical.os.OSSupport
import org.gridvise.logical.os.ProcInfo
import scala.sys.process._


/**
 * Windows support provided by http://www.antapex.org/winshells.txt
 */
class WindowsSupport extends OSSupport {

  override def getClasspathSeparator = ";"

  override def getJavaCommand = "java"

  def getProcessIdentifier(process: scala.sys.process.Process): String = {
    //TODO
    throw new Exception("not implemented yet")
  }
  override def getChildPid(pid: String):String = {
    throw new Exception("not implemented yet")
  }

  def stopProcess(processIdentifier: String) = {
    //TODO
    //throw new Exception("not implemented yet")
  }

  def exec(cmd: String): String = {
    //TODO
    throw new Exception("not implemented yet")
  }

  def threadDump(processIdentifier: String) {
    //TODO
    //throw new Exception("not implemented yet")
  }

  def setSystemPropery(name: String, value: String) {
    //throw new Exception("not implemented")
  }

  def isValidPid(pid: String): Boolean = {
   
    val v = ("tasklist /FI \"PID eq " + pid + "\"").!!
    val foundPid = (v.indexOf(pid) != -1)
    return foundPid
    
  }

  override def getCPUUsage(): List[ProcInfo] = {
    throw new Exception("getCPUUsage not implemented")
  }

  override def getCPUUsageHigherThen(percentage: Int): List[ProcInfo] = {
    throw new Exception("getCPUUsageHigherThen not implemented")

  }

  override def getUserName(): String = {
   
    val v = System.getenv("USERNAME")
    return v
  }

}