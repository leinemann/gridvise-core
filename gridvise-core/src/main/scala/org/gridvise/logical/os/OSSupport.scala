package org.gridvise.logical.os

abstract class OSSupport {

  def getClasspathSeparator(): String
  
  def getJavaCommand(): String
  
  def getProcessIdentifier(process: scala.sys.process.Process): String

  def stopProcess(processIdentifier: String)

  def exec(cmd: String): String
  
  def threadDump(processIdentifier: String)
  
  def setSystemPropery(name: String, value: String)
  
  def isValidPid(pid: String): Boolean
  
  def getChildPid(pid: String): String
  
  def getCPUUsage(): List[ProcInfo]

  def getCPUUsageHigherThen(percentage: Int):List[ProcInfo]

  def getUserName(): String
  
}