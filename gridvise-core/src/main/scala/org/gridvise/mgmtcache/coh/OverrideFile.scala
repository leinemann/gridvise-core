package org.gridvise.mgmtcache.coh
import org.gridvise.logical.ClusterConfigParser

object OverrideFile {

  def getClusterName() = "mgmt-"+ClusterConfigParser.clusterName+"-"+ClusterConfigParser.clusterMode
  def getDefaultMgmtPort() = 17028

  var overrideXml = <?xml version='1.0'?>
                    <coherence>
                      <cluster-config>
                        <member-identity>
                          <!-- Note: All members of the cluster must specify the same cluster name
                                                                in order to be allowed to join the cluster. -->
                          <cluster-name system-property="tangosol.coherence.cluster">{ getClusterName() }</cluster-name>
                          <site-name system-property="tangosol.coherence.site"></site-name>
                          <rack-name system-property="tangosol.coherence.rack"></rack-name>
                          <machine-name system-property="tangosol.coherence.machine"></machine-name>
                          <process-name system-property="tangosol.coherence.process"></process-name>
                          <member-name system-property="tangosol.coherence.member"></member-name>
                          <role-name system-property="tangosol.coherence.role"></role-name>
                          <priority system-property="tangosol.coherence.priority"></priority>
                        </member-identity>
                        <well-known-addresses>
                          <socket-address id="1">
                            <address system-property="tangosol.coherence.wka1"></address>
                            <port system-property="tangosol.coherence.wka1.port"></port>
                          </socket-address>
                          <socket-address id="2">
                            <address system-property="tangosol.coherence.wka2"></address>
                            <port system-property="tangosol.coherence.wka2.port"></port>
                          </socket-address>
                        </well-known-addresses>
                        <unicast-listener>
                          <address system-property="tangosol.coherence.localhost">localhost</address>
                          <port system-property="tangosol.coherence.localport">{ getDefaultMgmtPort() }</port>
                          <port-auto-adjust system-property="tangosol.coherence.localport.adjust">true</port-auto-adjust>
                        </unicast-listener>
                      </cluster-config>
                    </coherence>
}