package org.gridvise.mgmtcache.coh.invocation.tasks.runbook

import org.gridvise.logical.runbook.LocalRunBook
import org.gridvise.logical.os.MachineInfo

class RunBookLoadTask(runBookId: String) extends RemoteRunBookTask{

  setRunBookId(runBookId)

  override def execute(): String = {
    val runBookId = this.getRunBookId()
    val id = execute(LocalRunBook.loadRunBook(runBookId))
    "completed loading RunBook "+id +" on "+MachineInfo.getMachineName()
  }

}
