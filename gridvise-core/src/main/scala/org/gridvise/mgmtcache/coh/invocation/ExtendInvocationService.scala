package org.gridvise.mgmtcache.coh.invocation

import org.gridvise.coherence.cache.invocation.{AbstractRemoteTask, AbstractInvocationService}
import java.util


object ExtendInvocationService extends AbstractInvocationService{
  def getName() = "ExtendInvocationService"
  def getTargetServiceName = "InvocationService"

  override def queryOnAllMembers[R](task: AbstractRemoteTask[_ <: R]): util.Collection[R] = {
    super.queryOnAllMembers(getTargetServiceName, task)
  }
}
