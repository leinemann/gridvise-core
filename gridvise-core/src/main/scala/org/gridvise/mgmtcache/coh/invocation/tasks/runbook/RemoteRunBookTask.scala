package org.gridvise.mgmtcache.coh.invocation.tasks.runbook

import org.gridvise.logical.os.MachineInfo
import org.gridvise.coherence.cache.invocation.AbstractRemoteTask
import org.gridvise.mgmtcache.coh.entity.executioncontext.ExecutionContext
import java.util
import collection.JavaConverters._

abstract class RemoteRunBookTask  extends AbstractRemoteTask[String]{

  val PARAM_RUNBOOKID = "runBookId"
  val PARAM_ACTIVITYID = "activityId"
  val PARAM_EXECONTEXT = "executionContext"
  val PARAM_VARS = "vars"

  def setRunBookId(runBookId: String) {
    this.addParameter(PARAM_RUNBOOKID, runBookId)
  }

  def getRunBookId(): String = {
    this.getParameter(PARAM_RUNBOOKID)
  }

  def setActivityId(activityId: String) {
    this.addParameter(PARAM_ACTIVITYID, activityId)
  }

  def getActivityId(): String = {
    this.getParameter(PARAM_ACTIVITYID)
  }

  def setExcecutionContext(executionContext: ExecutionContext) {
    this.addParameter(PARAM_EXECONTEXT, executionContext)
  }

  def getExecutionContext(): ExecutionContext = {
    this.getParameter(PARAM_EXECONTEXT)
  }

  def setVars(vars: Map[String, String]) {
    val hm = new util.HashMap[String, String]()
    vars.foreach(e => hm.put(e._1,e._2 ))
    this.addParameter(PARAM_VARS, hm)
  }

  def getVars(): Map[String, String] = {
    //FIXME tidious and unsafe - just because scala map is not serializable
    val m: util.HashMap[String, String] = getParameter(PARAM_VARS)
    if(m.isInstanceOf[util.HashMap[String, String]]){
      val hm = m.asInstanceOf[util.HashMap[String, String]]
      hm.asScala.toMap[String, String]
    }else{
      Map[String, String]()
    }
  }

  def execute(f: => String): String = {
    val execContext = this.getExecutionContext()
    if(execContext.machines.contains(MachineInfo.getMachineName()))
      f
    else
      "execution context prevents execution on "+MachineInfo.getMachineName()
  }
}