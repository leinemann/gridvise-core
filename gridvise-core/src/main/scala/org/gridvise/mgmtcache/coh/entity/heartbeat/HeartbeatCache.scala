package org.gridvise.mgmtcache.coh.entity.heartbeat
import org.gridvise.coherence.cache.entity.AbstractCache
import com.tangosol.net.NamedCache
import com.tangosol.util.MapListener
import com.tangosol.util.MapEvent

object HeartbeatCache extends AbstractCache[String, Long] {

  override def getCacheName() = "heartbeat"

  override def addIndexes(namedCache: NamedCache) {}

  abstract class HeartbeatListener {
    def tick()
  }

  private class MapListenerAdapter(hl: HeartbeatListener) extends MapListener {
    override def entryUpdated(e: MapEvent) { hl.tick() }
    override def entryInserted(e: MapEvent) { hl.tick() }
    override def entryDeleted(e: MapEvent) {}
  }

  def fireHeartbeat(key: String) {
    put(key, System.currentTimeMillis())
  }

  def addHeartbeatListener(listener: HeartbeatListener, key: String) {
    this.addMapListener(new MapListenerAdapter(listener), key)
  }

  def removeHeartbeatListener(listener: HeartbeatListener, key: String) {
    this.removeMapListener(new MapListenerAdapter(listener), key)
  }
}