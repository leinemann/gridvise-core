package org.gridvise.mgmtcache.coh.entity.executioncontext

import org.slf4j.LoggerFactory


object ExecutionContextFactory {

  def logger = LoggerFactory.getLogger(this.getClass())
  
  def apply(machines: List[String]): ExecutionContext = {
    logger.info("Creating ExecutionContext with default user and machines " + machines)
    new DefaultExecutionContext(machines)
  }

  def apply(user:String, machines: List[String]): ExecutionContext = {
    logger.info("Creating ExecutionContext for user=%s and machines=%s".format(user, machines))
    //TODO authentication plugin
    new DefaultExecutionContext(machines)
  }

  private class DefaultExecutionContext(machines: List[String]) extends ExecutionContext("DefaultUser", machines)


}
