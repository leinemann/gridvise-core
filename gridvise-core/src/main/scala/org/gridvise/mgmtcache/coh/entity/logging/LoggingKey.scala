package org.gridvise.mgmtcache.coh.entity.logging
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey

class LoggingKey(var lineNumber: Long, var launchableKey: LaunchableKey) extends Serializable {
  override def toString() = "[lineNumber=" + lineNumber + "; " + this.launchableKey + "]"

  override def equals(other: Any): Boolean = {
    if (other == null) return false

    if (!other.isInstanceOf[LoggingKey]) return false

    var otherLoggingKey: LoggingKey = other.asInstanceOf[LoggingKey]
    if (otherLoggingKey != null) {
      if (this.lineNumber != otherLoggingKey.lineNumber) return false
      if (this.launchableKey != null)
        if (otherLoggingKey.launchableKey == null) return false
        else if (!this.launchableKey.equals(otherLoggingKey.launchableKey)) return false

      true
    } else {
      false
    }
  }

  override def hashCode() = {

    val prime: Int = 31
    var result: Int = 1

    result = prime * result + this.lineNumber.toInt

    var launchableKeyHash: Int = 0
    if (launchableKey != null) launchableKeyHash = launchableKey.hashCode()
    result = prime * result + (launchableKeyHash)

    result
  }
}