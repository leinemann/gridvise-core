package org.gridvise.mgmtcache.coh.invocation.tasks
import org.gridvise.LocalAPI
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableCache
import org.gridvise.logical.os.MachineInfo
import org.slf4j.LoggerFactory
import org.gridvise.util.AsyncInvoker

class RemoteStartTask extends RemoteLaunchableTask {
  
  def logger = LoggerFactory.getLogger(this.getClass())
  
  def execute(): String = {
    val launchableKeys: Seq[LaunchableKey] = this.getLaunchableKeys()
    logger.info("StartTask invoked with keys "+launchableKeys)
    if (launchableKeys == null) {
    	LocalAPI.start()
    } else {
          launchableKeys.foreach { key => start(key) }
    }
    "started"
  }

  def start(key: LaunchableKey) {
    logger.info("Starting... " + key)
    if(key.machineName.equals(MachineInfo.getMachineName())){
    	LaunchableCache.get(key).start()
    }
  }

}