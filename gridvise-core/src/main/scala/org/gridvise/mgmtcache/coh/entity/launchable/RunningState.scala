package org.gridvise.mgmtcache.coh.entity.launchable

object RunningState extends Enumeration {
  type RunningState = Value
  val Running, Stopped, SomeRunningSomeStopped = Value
}
