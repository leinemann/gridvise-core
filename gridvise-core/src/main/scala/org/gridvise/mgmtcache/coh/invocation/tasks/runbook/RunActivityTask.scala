package org.gridvise.mgmtcache.coh.invocation.tasks.runbook

import org.gridvise.logical.runbook.LocalRunBook

class RunActivityTask(runBookId: String, activityId: String) extends RemoteRunBookTask{

  setRunBookId(runBookId)
  setActivityId(activityId)


  override def execute(): String = {
     val runBookId = this.getRunBookId()
     val activityId = this.getActivityId()
     val vars = this.getVars()
     this.execute(LocalRunBook.runActivity(runBookId, activityId, vars))
   }
 }
