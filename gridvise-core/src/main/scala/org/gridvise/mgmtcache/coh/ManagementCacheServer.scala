package org.gridvise.mgmtcache.coh
import java.lang.System.getProperties
import java.net.URLClassLoader
import scala.actors.Actor
import org.gridvise.logical.messages.Start
import org.gridvise.mgmtcache.coh.entity.config.ConfigCache
import org.gridvise.LocalAPI
import com.tangosol.net.DefaultCacheServer
import org.springframework.context.support.ClassPathXmlApplicationContext
import org.slf4j.LoggerFactory

/**
 * The main entry point for gridvise-core agent
 */
object ManagementCacheServer extends Actor {

  /**
   * Add logging class for the main ManagementCacheServer
   */
  def logger = LoggerFactory.getLogger(this.getClass())
  
  final val CACHECONFIG_FILE_PROP = "tangosol.coherence.cacheconfig"
  final val CACHECONFIG_FILE_VALUE = "management-cache-config.xml"    
  
  def main(args: Array[String]): Unit = {
    
    //TODO hook this into some sort of life cycle of the ManagmentCluster
    // to avoid two different ManagementCacheServer from inserting this 
    logger.info("Starting gridvise-core cluster agent %s=%s...".format(CACHECONFIG_FILE_PROP, CACHECONFIG_FILE_VALUE))
    getProperties().put(CACHECONFIG_FILE_PROP, CACHECONFIG_FILE_VALUE)
    getProperties().put("localstorage.dist.mgmt", "true")
    getProperties().put("localstorage.invocation.mgmt", "true")

    this.start()
    this ! Start

    Thread.sleep(10000);

    val config = getProperties().getProperty("gridvise.config")
      
    logger.info("Attempting to load grid config from %s".format(config))

    val xmlBufferSource = scala.io.Source.fromFile(config)
    val xmlConfigString = xmlBufferSource.mkString
    xmlBufferSource.close()

    ConfigCache.putConfig(xmlConfigString)
    logger.info("inserted config")
    LocalAPI.initialize()
    
    logger.info("***************************************************")
    logger.info("******************* initialized *******************")
    logger.info("***************************************************")
    
    new ClassPathXmlApplicationContext("check-schedule.xml");
  }

  def act() {
    while (true) {
      receive {
        case Start => DefaultCacheServer.main(new Array[String](0))
      }
    }
  }
  
  def printlnCPInfo() {
    var sysClassLoader = ClassLoader.getSystemClassLoader().asInstanceOf[URLClassLoader];

        //Get the URLs
       var urls = sysClassLoader.getURLs();
       urls.foreach(cp => logger.info(cp+":"))
  }
}