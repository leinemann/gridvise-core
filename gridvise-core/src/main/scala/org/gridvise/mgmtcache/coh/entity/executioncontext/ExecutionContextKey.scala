package org.gridvise.mgmtcache.coh.entity.executioncontext

import java.util.UUID

class ExecutionContextKey extends Serializable{

  val uuid = UUID.randomUUID()

}

