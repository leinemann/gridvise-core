package org.gridvise.mgmtcache.coh.entity.events

import org.gridvise.event.Event
import org.gridvise.coherence.cache.entity.AbstractCache
import com.tangosol.net.NamedCache
import com.tangosol.util.extractor.ReflectionExtractor
import com.tangosol.util.MapEvent
import com.tangosol.util.MapListener
import com.tangosol.util.ValueExtractor
import org.slf4j.LoggerFactory

object EventCache extends AbstractCache[EventKey, Event] with MapListener {

  def logger = LoggerFactory.getLogger(this.getClass())
  val launchableKeyExtractor: ValueExtractor = new ReflectionExtractor("launchableKey")
  val dictionaryEntryExtractor: ValueExtractor = new ReflectionExtractor("dictionaryEntry")

  var listener = List[EventListener]()

  override def getCacheName() = "event"

  override def addIndexes(namedCache: NamedCache) {
  //  namedCache.addIndex(launchableKeyExtractor, false, null)
  //  namedCache.addIndex(dictionaryEntryExtractor, false, null)

    namedCache.addMapListener(this)
    logger.info("Registered event listener")
  }

  def store(event: Event) {
    val l = event.launchalbe
    put(event.eventKey(), event)
  }

  def addListener(eventListener: EventListener) {
    //FIXE is this thread safe???
    listener = eventListener :: listener
  }

  def entryDeleted(evt: MapEvent) {
    logger.info("Deleted: " + evt)
  }
  
  def entryInserted(evt: MapEvent) {
    logger.info("Inserted: %s notifying %s listeners".format(evt, listener.size, listener))
    listener.foreach(l => l.handleEvent(evt.getNewValue().asInstanceOf[Event]))
  }
  
  def entryUpdated(evt: MapEvent) {
    logger.info("Updated: " + evt)
  }

}