package org.gridvise.mgmtcache.coh.entity.threaddumps
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey
import java.util.Date

class ThreadDumpKey(launchableKey: LaunchableKey, generationDate: Date) extends Serializable{
	override def toString() = launchableKey.toString() +"@" + generationDate
}