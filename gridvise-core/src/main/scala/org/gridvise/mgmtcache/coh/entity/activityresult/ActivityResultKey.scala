package org.gridvise.mgmtcache.coh.entity.activityresult

import java.util.UUID

class ActivityResultKey(activityId:String, runBookId: String) extends Serializable{

  val uuid = UUID.randomUUID()

}

