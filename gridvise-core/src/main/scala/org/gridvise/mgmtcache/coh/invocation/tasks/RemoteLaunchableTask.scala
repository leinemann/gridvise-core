package org.gridvise.mgmtcache.coh.invocation.tasks
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey

import org.gridvise.coherence.cache.invocation.AbstractRemoteTask

abstract class RemoteLaunchableTask extends AbstractRemoteTask[String]{

  val PARAM_LAUNCHABLE_KEYS = "launchableKeys"

  def setKeys(keys: List[LaunchableKey]) {
    this.addParameter(PARAM_LAUNCHABLE_KEYS, keys.asInstanceOf[Serializable])
  }
  
  def getLaunchableKeys(): Seq[LaunchableKey] = {
    this.getParameter(PARAM_LAUNCHABLE_KEYS)
  }

}