package org.gridvise.mgmtcache.coh.entity.config.cachestore
import org.gridvise.mgmtcache.coh.entity.config.ConfigKey
import com.tangosol.net.cache.AbstractCacheStore
import org.gridvise.coherence.cache.config.Configuration
import org.slf4j.LoggerFactory

class ConfigCacheStore extends AbstractCacheStore {

  def logger = LoggerFactory.getLogger(this.getClass())
  
  override def load(o: Any) = {
    logger.info("Loading " + o)
    val oKey = o.asInstanceOf[ConfigKey]
    Configuration.get(oKey.fileName, oKey.propertyName)
  }

  override def store(k: Any, v: Any) = { }

}