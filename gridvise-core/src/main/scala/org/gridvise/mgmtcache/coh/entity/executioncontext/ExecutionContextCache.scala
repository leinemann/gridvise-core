package org.gridvise.mgmtcache.coh.entity.executioncontext

import org.gridvise.coherence.cache.entity.AbstractCache
import com.tangosol.net.NamedCache

object ExecutionContextCache extends AbstractCache[ExecutionContextKey, ExecutionContext]  {

  override def getCacheName() = "executioncontext"
  override def addIndexes(namedCache: NamedCache) {}

  def putExecutionContext(executionContext : ExecutionContext) {
      this.put(new ExecutionContextKey, executionContext)
  }



}
