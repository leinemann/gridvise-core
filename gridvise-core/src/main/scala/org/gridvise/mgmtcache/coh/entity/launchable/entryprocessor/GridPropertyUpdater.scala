package org.gridvise.mgmtcache.coh.entity.launchable.entryprocessor
import org.gridvise.logical.Launchable
import org.gridvise.xmlbindings.DictionaryEntryable

import com.tangosol.util.processor.AbstractProcessor
import com.tangosol.util.InvocableMap

class GridPropertyUpdater(val dictionaryEntry: DictionaryEntryable, value: String) extends AbstractProcessor {

  def process(entry: InvocableMap.Entry): Object = {
    if (entry.isPresent()) {
      var l = entry.getValue().asInstanceOf[Launchable]
      l.addGridProperty(this.dictionaryEntry, this.value)
      entry.setValue(l)
      "updated " + entry.getKey() + ". Set " + this.dictionaryEntry.getClass() + " to " + this.value
    } else {
      "entry for " + entry.getKey() + " not present"
    }
  }

}