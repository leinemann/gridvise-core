package org.gridvise.mgmtcache.coh.entity.executioncontext

abstract class ExecutionContext(val user:String, val machines: List[String]) extends Serializable
