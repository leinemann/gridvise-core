package org.gridvise.mgmtcache.coh.entity.gridsequence.entryprocessor

import org.gridvise.logical.gridsequence.AllocationRange

import com.tangosol.util.processor.AbstractProcessor
import com.tangosol.util.InvocableMap

class GridSequenceAllocationProcessor extends AbstractProcessor {

  //FIXME via config
  val allocationSize: Long = 1000
  val initialAllocationRange = new AllocationRange(0, allocationSize)

  def process(entry: InvocableMap.Entry) = {
    if (entry.isPresent()) {
      var currentMax = entry.getValue().asInstanceOf[Long]
      var nextMax = currentMax + allocationSize
      if(nextMax < 0){//in case of crossing Long.MaxValue start from 0 again
        entry.setValue(allocationSize)
        initialAllocationRange
      }else{
    	  entry.setValue(nextMax)
        new AllocationRange(currentMax + 1, nextMax)
      }
    } else {
      entry.setValue(allocationSize)
      initialAllocationRange
    }
  }

}