package org.gridvise.mgmtcache.coh.entity.threaddumps
import org.gridvise.logical.ThreadDump

import org.gridvise.coherence.cache.entity.AbstractCache
import com.tangosol.net.NamedCache

object ThreadDumpCache extends AbstractCache[ThreadDumpKey, ThreadDump]  {
  
  def addIndexes(namedCache: NamedCache){
    
  }
  
  def getCacheName(): String = "threaddump"
	
}