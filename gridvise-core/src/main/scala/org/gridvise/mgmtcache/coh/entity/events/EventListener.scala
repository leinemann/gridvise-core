package org.gridvise.mgmtcache.coh.entity.events
import org.gridvise.event.Event

abstract class EventListener {
  
  def handleEvent(event: Event)

}