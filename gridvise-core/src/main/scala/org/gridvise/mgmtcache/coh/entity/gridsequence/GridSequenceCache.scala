package org.gridvise.mgmtcache.coh.entity.gridsequence

import org.gridvise.logical.gridsequence.AllocationRange
import org.gridvise.mgmtcache.coh.entity.gridsequence.entryprocessor.GridSequenceAllocationProcessor

import org.gridvise.coherence.cache.entity.AbstractCache
import com.tangosol.net.NamedCache

object GridSequenceCache extends AbstractCache[String, Long] {

  val allocationProcessor = new GridSequenceAllocationProcessor()
  val key = "sequence"

  override def getCacheName() = "gridsequence"
  override def addIndexes(namedCache: NamedCache) {}

  def nexRange(): AllocationRange = {
    invoke(key, allocationProcessor).asInstanceOf[AllocationRange]
  }


}