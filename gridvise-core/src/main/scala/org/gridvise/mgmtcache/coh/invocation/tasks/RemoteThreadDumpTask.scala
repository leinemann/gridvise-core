package org.gridvise.mgmtcache.coh.invocation.tasks
import java.util.Date

import org.gridvise.logical.ThreadDump
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableCache
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey
import org.gridvise.coherence.cache.invocation.AbstractRemoteTask

class RemoteThreadDumpTask extends AbstractRemoteTask[List[ThreadDump]] {

  val PARAM_LAUNCHABLE_KEYS = "launchableKeys"

  def execute(): List[ThreadDump] = {
    var launchableKeys: Seq[LaunchableKey] = this.getParameter(PARAM_LAUNCHABLE_KEYS)
    var result: List[ThreadDump] = List()
    val generationTime = new Date()
    if (launchableKeys != null) {
      launchableKeys.foreach { key => 
        val dump = threadDump(generationTime, key)
        result = dump :: result }
    }
    result
  }

  def threadDump(generationTime: Date, key: LaunchableKey): ThreadDump = {
      var l = LaunchableCache.get(key)
      l.threadDump(generationTime)
  }

  def setKeys(keys: List[LaunchableKey]) {
    this.addParameter(PARAM_LAUNCHABLE_KEYS, keys.asInstanceOf[Serializable])
  }

}