package org.gridvise.mgmtcache.coh.invocation.tasks
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableCache
import org.gridvise.logical.os.OSOperations

class RemoteReEvalTask extends RemoteLaunchableTask {

  def execute(): String = {
    val launchables = LaunchableCache.getLaunchablesOnThisMachine()
    launchables.foreach(l =>
      if (OSOperations.isValidPid(l.processIdentifier)) {
        if (!l.isRunning()) {
          l.running = true
          LaunchableCache.putLaunchable(l)
        }
      } else {
        if (l.isRunning()) {
          l.running = false
          LaunchableCache.putLaunchable(l)
        }
      })
    "re-evaluated"
  }

}