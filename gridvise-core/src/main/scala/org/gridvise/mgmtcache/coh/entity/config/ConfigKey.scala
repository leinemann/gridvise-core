package org.gridvise.mgmtcache.coh.entity.config

class ConfigKey(val fileName: String, val propertyName: String) extends Serializable {
  override def toString() = "[ConfigKey:"+fileName+"->"+propertyName+"]"

}