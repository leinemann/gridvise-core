package org.gridvise.mgmtcache.coh.entity.launchable
import scala.collection.JavaConversions.collectionAsScalaIterable
import scala.collection.JavaConversions.seqAsJavaList
import org.gridvise.logical.Launchable
import org.gridvise.coherence.cache.entity.AbstractCache
import org.gridvise.coherence.cache.entity.ICache
import com.tangosol.net.NamedCache
import com.tangosol.util.filter.EqualsFilter
import org.gridvise.xmlbindings.DictionaryEntryable
import org.gridvise.mgmtcache.coh.entity.launchable.entryprocessor.GridPropertyUpdater
import org.slf4j.LoggerFactory

object LaunchableCache extends AbstractCache[LaunchableKey, Launchable] {
  
  def logger = LoggerFactory.getLogger(this.getClass())

  override def getCacheName() = "launchable"
  override def addIndexes(namedCache: NamedCache) {}

  def putLaunchable(launchable: Launchable): Any = {
    put(launchable.getLaunchableKey(), launchable)
  }

  def removeLaunchable(launchable: Launchable): Any = {
    remove(launchable.getLaunchableKey())
  }
  
  def getRunningState(launchableKeys: List[LaunchableKey]): RunningState.Value = {
    var v = this.distictValues(launchableKeys, "running")
    if(v.size()==1){
      if(v.iterator().next()){
        return RunningState.Running
      }else{
        return RunningState.Stopped
      }
    }else if(v.size() == 2){
      return RunningState.SomeRunningSomeStopped
    }
    RunningState.Stopped
  }
  
  def setGridProperty(lk: LaunchableKey, dictionaryEntry: DictionaryEntryable, value: String) {
    val ep = new GridPropertyUpdater(dictionaryEntry, value);
    logger.info("%s".format(invoke(lk, ep)))
  }
  
  def getLaunchablesOnThisMachine(): List[Launchable] = {
    //TODO
    this.values().toList
  }

//  def getLaunchablesForNodeGroup(nodeGroupName: String) = {
//    this.keySet(getNodeGroupFilter(nodeGroupName))
//  }
//
  def getLaunchablesForJvmConfig(configName: String) = {
    this.keySet(getJvmConfigFilter(configName))
  }

  def getLaunchableKeysForNodeGroup(nodeGroupName: String) = {
	this.keySet(getNodeGroupFilter(nodeGroupName)).toList;
  }
  
  def getLaunchableForPid(pid: String) = {
    get(keySet(getPIDFilter(pid)).head)

//    val launchables = this.keySet(getPIDFilter(pid))
//    if(launchables.size() == 1 ){
//      launchables.head
//    }
  }

  def getLaunchableKeysForJvmConfig(configName: String) = {
	this.keySet(getJvmConfigFilter(configName)).toList;
  }

  def getInstance(): ICache[LaunchableKey, Launchable] = {
    this
  }
  
  def getNodeGroupFilter(name: String) =  new EqualsFilter("nodeGroupName", name);
  def getJvmConfigFilter(name: String) =  new EqualsFilter("configName", name);
  def getPIDFilter(pid: String) =  new EqualsFilter("processIdentifier", pid);

}