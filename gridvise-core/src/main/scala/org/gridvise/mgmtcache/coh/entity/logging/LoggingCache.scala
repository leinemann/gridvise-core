package org.gridvise.mgmtcache.coh.entity.logging

import scala.collection.JavaConversions._
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey
import org.gridvise.coherence.cache.entity.AbstractCache
import com.tangosol.net.NamedCache
import com.tangosol.util.filter.EqualsFilter
import com.tangosol.util.ValueExtractor
import com.tangosol.util.extractor.KeyExtractor

object LoggingCache extends AbstractCache[LoggingKey, String] {
  
  val launchableKeyExtractor: ValueExtractor = new KeyExtractor("launchableKey")
  val lineNumberExtractor: ValueExtractor = new KeyExtractor("lineNumber")
  
  override def getCacheName() = "logging"
    
  override def addIndexes(namedCache: NamedCache) {
    namedCache.addIndex(launchableKeyExtractor, false, null)
    namedCache.addIndex(lineNumberExtractor,true, null)
  }
  
  def getLog(key: LaunchableKey): String = {
    val f = new EqualsFilter(this.launchableKeyExtractor, key)
    val unsortedKeys = keySet(f)
    var sortedKeys =  unsortedKeys.toList.sortWith((a, b) => a.lineNumber < b.lineNumber)
    val lineMap = getAll(unsortedKeys)
    var result = ""
    sortedKeys.foreach(a => {
      result += "\n"+lineMap(a)
    })
    result
  }

}