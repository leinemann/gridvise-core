package org.gridvise.mgmtcache.coh.invocation.tasks
import org.gridvise.logical.os.MachineInfo
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableCache
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey
import org.gridvise.LocalAPI
import org.slf4j.LoggerFactory
import org.gridvise.util.AsyncInvoker

class RemoteStopTask extends RemoteLaunchableTask {

  def logger = LoggerFactory.getLogger(this.getClass())
  
  def execute(): String = {
    val launchableKeys: Seq[LaunchableKey] = this.getLaunchableKeys()
    if (launchableKeys == null) {
    	LocalAPI.stop()
    } else {
          launchableKeys.foreach { key => stop(key)}
    }
    "stopped"
  }

  def stop(key: LaunchableKey) {
    logger.info("Stopping... " + key)
    if(key.machineName.equals(MachineInfo.getMachineName())){
    	LaunchableCache.get(key).stop()
    }
  }


}