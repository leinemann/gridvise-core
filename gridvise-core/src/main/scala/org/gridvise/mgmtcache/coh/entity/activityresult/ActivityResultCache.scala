package org.gridvise.mgmtcache.coh.entity.activityresult

import org.gridvise.coherence.cache.entity.AbstractCache
import com.tangosol.net.NamedCache
import org.gridvise.logical.runbook.activity.ActivityContext
import com.tangosol.util.QueryHelper
import java.util

object ActivityResultCache extends AbstractCache[ActivityResultKey, ActivityResult]  {

  override def getCacheName() = "activity"
  override def addIndexes(namedCache: NamedCache) {
    //TODO add indexes for runbook and activity id

  }

  def putActivity(context: ActivityContext, result: String) {
      this.put(new ActivityResultKey(context.runBookId, context.id), new ActivityResult(context, result))
  }


  def getActivities(runBookId: String, activityId: String): util.Collection[ActivityResult]= {
    val f = QueryHelper.createFilter("context().runBookId() = '"+runBookId+"' and context().id()='"+activityId+"'")
    values(f).values()
  }

  def getActivities(runBookId: String): util.Collection[ActivityResult]= {
    val f = QueryHelper.createFilter("context().runBookId() = '"+runBookId+"'")
    values(f).values()
  }


}
