package org.gridvise.mgmtcache.coh.entity.activityresult

import org.gridvise.logical.runbook.activity.ActivityContext

class ActivityResult(val context: ActivityContext, val result: String) extends Serializable

