package org.gridvise.mgmtcache.coh.entity.launchable

class LaunchableKey(var machineName: String, var clusterId: Long) extends Serializable {

  override def toString(): String = {
    "[machineName=" + this.machineName + ", clusterId=" + clusterId + "]"
  }

    override def equals(other: Any): Boolean = {
      if (other == null) return false
  
      if (!other.isInstanceOf[LaunchableKey]) return false
  
      var otherLaunchableKey: LaunchableKey = other.asInstanceOf[LaunchableKey]
      if (otherLaunchableKey != null) {
        if (this.clusterId != otherLaunchableKey.clusterId) return false
        if (this.machineName != null)
          if (otherLaunchableKey.machineName == null) return false
          else if (!this.machineName.equals(otherLaunchableKey.machineName)) return false
  
        true
      } else {
        false
      }
    }
  
    override def hashCode() = {
  
      val prime: Int = 31
      var result: Int = 1
  
      result = prime * result + this.clusterId.toInt
  
      var machineHash: Int = 0
      if (machineName != null) machineHash = machineName.hashCode()
      result = prime * result + (machineHash)
  
      result
    }
}

