package org.gridvise.mgmtcache.coh.entity.events



class EventKey(var id: Long, val eventType: String, val hostName: String, val nodeGroupName: String, val configName: String) extends Serializable {

  override def toString() = "[id=" + id + "]"

  override def equals(other: Any): Boolean = {

    if (other == null) return false

    if (!other.isInstanceOf[EventKey]) return false

    var otherEventgKey: EventKey = other.asInstanceOf[EventKey]
    if (otherEventgKey != null) {
      if (this.id != otherEventgKey.id) return false

      true
    } else {
      false
    }
  }

  override def hashCode() = {

    val prime: Int = 31
    var result: Int = 1

    prime * result + this.id.toInt
  }
}