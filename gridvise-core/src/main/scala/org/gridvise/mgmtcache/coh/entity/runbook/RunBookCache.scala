package org.gridvise.mgmtcache.coh.entity.runbook

import org.gridvise.coherence.cache.entity.AbstractCache
import com.tangosol.net.NamedCache
import org.gridvise.xmlbindings.RunBook

object RunBookCache extends AbstractCache[String, RunBook]  {

  override def getCacheName() = "runbook"
  override def addIndexes(namedCache: NamedCache) {}

  def putRunBook(runBook : RunBook) {
      this.put(runBook.id, runBook)
  }

  def getRunBook(id: String) = {
    this.get(id)
  }


}
