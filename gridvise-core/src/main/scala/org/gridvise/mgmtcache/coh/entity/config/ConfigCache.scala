package org.gridvise.mgmtcache.coh.entity.config
import org.gridvise.coherence.cache.entity.AbstractCache
import com.tangosol.net.NamedCache

object ConfigCache extends AbstractCache[ConfigKey, String] {

  private val default = "default"
  
  private val configKey = new ConfigKey(default,"jvms.config")

  override def getCacheName() = "config"
    
  override def addIndexes(namedCache: NamedCache){ }
  
  def putConfig(configXml: String) = {
    put(configKey, configXml)
  }
  
  def getConfig(): String = {
    get(configKey)
  }
  
  def get(file: String, name: String):String = get(new ConfigKey(file, name))
  
  def getCPUCheckThreashold() = get("checks.properties","os-checks.cpucheck.threashold").toInt
}