package org.gridvise.mgmtcache.coh.invocation.tasks


import org.gridvise.coherence.cache.invocation.AbstractRemoteTask

class RemoteFunctionTask[R](func: => R) extends AbstractRemoteTask[R]{

  def execute()  = func

}