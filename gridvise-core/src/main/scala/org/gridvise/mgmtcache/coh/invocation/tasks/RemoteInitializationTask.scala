package org.gridvise.mgmtcache.coh.invocation.tasks

import org.gridvise.LocalAPI
import org.gridvise.logical.os.MachineInfo
import org.gridvise.mgmtcache.coh.entity.config.ConfigCache

class RemoteInitializationTask extends RemoteLaunchableTask {
  
  def execute(): String = {
    LocalAPI.initialize()
    "initialized on " + MachineInfo.getMachineName()
  }

}