package org.gridvise.mgmtcache.coh.invocation.tasks.runbook

import org.gridvise.logical.runbook.LocalRunBook

class RunBookTask(runBookId: String) extends RemoteRunBookTask{

  setRunBookId(runBookId)

   override def execute(): String = {
     val runBookId = this.getRunBookId()
     val vars = this.getVars()
     this.execute(LocalRunBook.runBook(runBookId, vars))
   }
 }
