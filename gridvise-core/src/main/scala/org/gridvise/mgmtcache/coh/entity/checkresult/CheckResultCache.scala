package org.gridvise.mgmtcache.coh.entity.checkresult
import org.gridvise.coherence.cache.entity.AbstractCache
import com.tangosol.net.NamedCache
import org.gridvise.event.streams.CheckResult
import org.gridvise.event.streams.CheckId

object CheckResultCache extends AbstractCache[CheckId, CheckResult] {

  override def getCacheName() = "checkresult"
    
  override def addIndexes(namedCache: NamedCache) {}
  
  def store(checkResult: CheckResult){
    put(checkResult.id, checkResult)
  }

}