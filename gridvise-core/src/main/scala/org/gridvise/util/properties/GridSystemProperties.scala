package org.gridvise.util.properties
import scala.sys.SystemProperties

object GridSystemProperties extends SystemProperties {

  def gridDictionaryFile() = {
    dealWithXmlFileSysProp("grid-dictionary")
  }

  def jvmDictionaryFile() = {
    dealWithXmlFileSysProp("jvm-dictionary")
  }

  def dealWithXmlFileSysProp(name: String) = {
    val p = get(name)
    if (p.isEmpty) "dictionaries/"+name+".xml" else p
  }

}