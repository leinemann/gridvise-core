package org.gridvise.util.jmx

class Condition(val address: ValueAddress, threashold: Integer, comparison: (Integer, Integer) => Boolean) {

  def violations(values: Map[String, Integer]) = {
    values.filter( e => comparison(threashold, e._2))
  }

}