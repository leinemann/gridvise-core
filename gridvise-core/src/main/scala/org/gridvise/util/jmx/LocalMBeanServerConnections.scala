package org.gridvise.util.jmx
import java.io.File

import scala.collection.JavaConversions._

import com.sun.tools.attach.VirtualMachine

import javax.management.remote.JMXConnectorFactory
import javax.management.remote.JMXServiceURL
import javax.management.MBeanServerConnection
import javax.management.ObjectInstance
import javax.management.ObjectName
import javax.management.Query

object LocalMBeanServerConnections {

  private val CONNECTOR_ADDRESS =
    "com.sun.management.jmxremote.localConnectorAddress";

  //FIXME clean up the pool!!
  val pool = scala.collection.mutable.Map[String, MBeanServerConnection]()

  def getConnection(pid: String): MBeanServerConnection = {
    if (pool.contains(pid)) {
      pool(pid)
    } else {
      createConnectionForPid(pid)
    }
  }

  def removeConnection(pid: String) {
    pool.remove(pid)
  }

  def createConnectionForPid(pid: String): MBeanServerConnection = {
    val target = getURLForPid(pid);
    val connector = JMXConnectorFactory.connect(target);
    val c = connector.getMBeanServerConnection();
    pool(pid) = c
    c
  }

  def getURLForPid(pid: String): JMXServiceURL = {
    val vm = VirtualMachine.attach(pid);
    var connectorAddress =
      vm.getAgentProperties().getProperty(CONNECTOR_ADDRESS);
    if (connectorAddress == null) {
      val agent = vm.getSystemProperties().getProperty("java.home") +
        File.separator + "lib" + File.separator + "management-agent.jar";
      vm.loadAgent(agent);
      connectorAddress =
        vm.getAgentProperties().getProperty(CONNECTOR_ADDRESS);
    }
    new JMXServiceURL(connectorAddress);
  }

  def getValues(address: ValueAddress, pid: String) = {
    val on = new ObjectName(address.addressSpace);
    val name = Query.attr(address.name);

    val c = getConnection(pid)

    val beans = c.queryMBeans(on, null);
    //println("found " + beans.size() + " beans");

    val m = beans.toList.asInstanceOf[List[ObjectInstance]].map(b => {
      (b.getObjectName().toString(), c.getAttribute(b.getObjectName(), address.name).toString().toInt)
    }) toMap
    
    m

  }

}