package org.gridvise.util.jmx
import javax.management.ObjectName
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableCache
import javax.management.Query
import javax.management.MBeanServerConnection
import scala.collection.JavaConversions._
import javax.management.ObjectInstance
import org.slf4j.LoggerFactory

object Test {
  
  def logger = LoggerFactory.getLogger(this.getClass())
  def testLister() = {
    LaunchableCache.getLaunchablesOnThisMachine().foreach(l => {
      if (l.isRunning()) {
        val i = l.getClusterId()
        testQuery(LocalMBeanServerConnections.getConnection(l.processIdentifier), i.toString())
      }

    })
  }

  def testQuery(c: MBeanServerConnection, id: String) {
    val on = new ObjectName("Coherence:type=Service,name=*,nodeId=*");
    val name = Query.attr("ThreadIdleCount");
    val exp1 = Query.not(Query.gt(name, Query.value(-1)));

    val beans = c.queryMBeans(on, null);
    logger.info("Found %s beans".format(beans.size()))

    beans.toList.asInstanceOf[List[ObjectInstance]].foreach(b => {
      logger.info("mbean info => " + c.getMBeanInfo(b.getObjectName()))
      val att = c.getAttribute(b.getObjectName(), "ThreadIdleCount");
      logger.info("att " + att);

    })

  }

  //          if (l.processIdentifier.equals("71458")) {
  //          val c = LocalMBeanServerConnections.getConnection(l.processIdentifier)
  ////          val objectName = new ObjectName("Coherence:type=Service,name=*,nodeId=*");
  //          val objectName = new ObjectName("Coherence:type=Service,name=DistributedCacheForSubscriptions,nodeId=1");
  //          val mbeanProxy = JMX.newMBeanProxy(c, objectName,
  //            classOf[DynamicMBean]).asInstanceOf[DynamicMBean];
  //          println("what's this?:" + mbeanProxy)
  ////          val oi = c.createMBean("com.tangosol.coherence.component.manageable.modelAdapter.ServiceMBean", objectName);
  ////          println("test mbean "+oi)
  //          testQuery(c)
  //          val changeFilter = new AttributeChangeNotificationFilter();
  //          changeFilter.enableAttribute("ThreadIdleCount");
  //          //println(" ThreadIdleCout ?!?! = "+mbeanProxy.getAttribute("ThreadIdleCount"))
  //          
  //          
  ////          val delegateName = ObjectName.getInstance("JMImplementation:type=MBeanServerDelegate");
  //          val delegateName = ObjectName.getInstance("Coherence:type=Service,name=*,nodeId=*");
  // 
  //          c.addNotificationListener(objectName, new AttributeChangeListener(), changeFilter, null);
  // 
  //          //c.addNotificationListener(objectName, new AttributeChangeListener(), changeFilter, null)
  //
  //        }

}