package org.gridvise.util
import scala.xml.XML
import org.gridvise.xmlbindings.DictionaryConfig
import org.gridvise.xmlbindings.GridDictionary
import org.gridvise.xmlbindings.GridEventDictionary
import org.gridvise.xmlbindings.JvmDictionary
import org.gridvise.xmlbindings.LogEventDictionary
import org.gridvise.xmlbindings.MemberId
import org.slf4j.LoggerFactory

object Dictionary {

  def logger = LoggerFactory.getLogger(this.getClass())
  
  var jvmDictionaryV: JvmDictionary = _
  var gridDictionaryV: GridDictionary = _
  var logEventDictionaryV: LogEventDictionary = _
  var gridEventDictionaryV: GridEventDictionary = _

  init()

  def init() {

    //FIXME !!
    val file = "dictionary-config.xml"
    logger.info("Initializing dictionaries as per " + file)

    val xml = XML.load(getClass().getClassLoader().getResourceAsStream(file))

    logger.info("Xml" + xml )
    val dicConfig = scalaxb.fromXML[DictionaryConfig](xml)
    logger.info("Dictionary config read successfully")
    dicConfig.Dictionary.foreach(a => init(a.className, a.fileName))
  }

  def init(className: String, fileName: String) {
    logger.info("Loading dictionary for class %s from file".format(className, fileName))
    val xml = XML.load(getClass().getClassLoader().getResourceAsStream(fileName))

    //FIXME !!
    if (className.endsWith("GridDictionary")) {
      gridDictionaryV = scalaxb.fromXML[GridDictionary](xml)
    } else if(className.endsWith("LogEventDictionary")){
      logEventDictionaryV = scalaxb.fromXML[LogEventDictionary](xml)
    } else if(className.endsWith("GridEventDictionary")){
      gridEventDictionaryV = scalaxb.fromXML[GridEventDictionary](xml)
    } else {
      jvmDictionaryV = scalaxb.fromXML[JvmDictionary](xml)
    }
  }

  def jvmDictionary(): JvmDictionary = {
    jvmDictionaryV
  }

  def gridDictionary(): GridDictionary = {
    MemberId
    gridDictionaryV
  }

  def logEventDictionary(): LogEventDictionary = {
    logEventDictionaryV
  }

  def gridEventDictionary(): GridEventDictionary = {
    gridEventDictionaryV
  }

}