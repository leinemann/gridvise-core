package org.gridvise.util
import org.gridvise.coherence.cache.scheduling.JobScheduler
import org.quartz.JobKey
import org.quartz.Job

object Scheduler {

  def schedule(clazz: Class[Job], name: String, intervalInSecond: Integer) {
    var job = createJob(clazz, name)
    var trigger = createTrigger(name, intervalInSecond)
    JobScheduler.schedule(job, trigger)
  }

  def unschedule(clazz: Class[Job], name: String, f: JobKey => Unit) {
    var job = createJob(clazz, name)
    JobScheduler.unschedule(job.getKey())
  }

  private def createJob(clazz: Class[Job], name: String) = org.quartz.JobBuilder.newJob(clazz).withIdentity(name, "heartbeat").build();

  private def createTrigger(name: String, intervalInSecond: Integer) = {
    org.quartz.TriggerBuilder.newTrigger()
      .withIdentity(name, "heartbeat")
      .startNow()
      .withSchedule(
        org.quartz.SimpleScheduleBuilder.simpleSchedule()
          .withIntervalInSeconds(
            intervalInSecond)
          .repeatForever())
      .build();
  }
}