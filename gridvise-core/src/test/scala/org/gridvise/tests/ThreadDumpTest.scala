package org.gridvise.tests

import scala.io.Source
import org.gridvise.logical.ThreadDump
import org.scalatest.FunSuite
import org.slf4j.LoggerFactory

class ThreadDumpTest extends FunSuite {

  def logger = LoggerFactory.getLogger(this.getClass())
  
  test("parse thread dump in individual stacks") {
    
    val source = Source.fromFile("/Users/christoph/git/gr1d/ClusterJVMs/src/test/resources/threadDump/sample-threaddump.txt")
    val dump = source.mkString
    source.close()

    var threadDump = new ThreadDump(dump, "Test", "Test", "Test")

    val threads = threadDump.splitByThread()
    
    threadDump.filterRunnable().foreach(s => assert(s.contains("RUNNABLE")))
    
    logger.info("Runnables %s".format(threadDump.filterRunnable().size))

  }

  test("name can be extracted correcty form first line") {
    val firstLine = "\"Proxy:ExtendTcpProxyService:TcpAcceptor\" daemon prio=5 tid=7fa17685c000 nid=0x117acf000 in Object.wait() [117ace000]"
    val expectedName = "Proxy:ExtendTcpProxyService:TcpAcceptor"
    val threadDump = new ThreadDump("Test", "Test", "Test", "Test")
    val name = threadDump.getThreadName(firstLine);

    if (name.equals(expectedName)) {
      //FIXME do proper asserts
      logger.info("passed")
    }

  }

}