package org.gridvise.tests


import scala.collection.mutable.HashSet
import scala.collection.mutable.Stack
import scala.collection.mutable.SynchronizedSet
import org.gridvise.logical.gridsequence.LongGridSequence
import org.gridvise.logical.logbroker.LogBroker
import org.gridvise.logical.os.OSOperations
import org.gridvise.logical.{JavaLaunchable, Launchable}
import org.gridvise.mgmtcache.coh.entity.config.ConfigCache
import org.gridvise.mgmtcache.coh.entity.events.EventKey
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableCache
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey
import org.gridvise.mgmtcache.coh.entity.logging.LoggingCache
import org.gridvise.mgmtcache.coh.entity.logging.LoggingKey
import org.gridvise.util.jmx.Test
import org.gridvise.util.AsyncInvoker
import org.gridvise.util.Dictionary
import org.gridvise.xmlbindings._
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.gridvise.mgmtcache.coh.invocation.tasks.{RemoteStopTask, RemoteStartTask}
import com.tangosol.net.CacheFactory
import org.gridvise.xmlbindings.DictionaryEntry
import scala.Some
import org.slf4j.LoggerFactory

@RunWith(classOf[JUnitRunner])
class TestLists extends FunSuite {
  
  def logger = LoggerFactory.getLogger(this.getClass)

  test("creating a list") {
    val list = List[Int]()
    val list2 = 1 :: 1 :: list
    logger.info("%s".format(list))
    logger.info("%s".format(list2))
  }

  test("pop is invoked on an empty stack") {

    val emptyStack = new Stack[Int]
    intercept[NoSuchElementException] {
      emptyStack.pop()
    }
    assert(emptyStack.isEmpty)
  }

  test("logsubscriber should be removed after unsubscribing") {
    logger.info("creating logsubscriber")
    val l = new LaunchableKey("machine", 2)
    val s = LogBroker.subscribe(l, Dictionary.gridDictionary().MemberId)
    assert(LogBroker.subscribers.size == 1)
    LogBroker.unsubscribe(l, s)
    assert(LogBroker.subscribers.size == 0)
  }

  test("logsubscriber should have logstatements after subscription") {
    logger.info("testing log subscriber")
    val l = new LaunchableKey("machineA", 2)
    val s = LogBroker.subscribe(l, Dictionary.jvmDictionary().EndOfStacktrace)
    LogBroker.handleInfoLine(l, "VM Thread")
    assert(LogBroker.subscribers.size == 0)
    assert(s.out.indexOf("VM Thread") != -1)
  }

  test("equals/hashCode method of LaunchableKey should match tow instances with same id/servernames") {
    val k1 = new LaunchableKey("test", 1)
    val k2 = new LaunchableKey("test", 1)
    assert(k1.equals(k2))
    assert(k1.hashCode() == k2.hashCode())
  }

  test("equals/hashCode method of LaunchableKey should not match if two instances have different id/servernames") {
    var k1 = new LaunchableKey("test", 1)
    var k2 = new LaunchableKey("test", 2)
    assert(!k1.equals(k2))
    assert(k1.hashCode() != k2.hashCode())
    k1 = new LaunchableKey("test1", 1)
    k2 = new LaunchableKey("test2", 1)
    assert(!k1.equals(k2))
    assert(k1.hashCode() != k2.hashCode())
  }

  test("jvm dictionary should return VM Thread ") {
    logger.info(Dictionary.jvmDictionary.EndOfStacktrace.lineMatch)
    assert(Dictionary.jvmDictionary.EndOfStacktrace.lineMatch.equals("VM Thread"))
  }

  test("jvm dictionary should have java command ") {
    logger.info(Dictionary.jvmDictionary.JavaCommand.valueAttribute)
    assert(Dictionary.jvmDictionary.JavaCommand.valueAttribute.endsWith("java"))
  }

  test("CallbackUnsubscribeingLogSubscriber should extract member id and unsubscribe") {
    var k1 = new LaunchableKey("test", 1)
    val s = LogBroker.subscribeCallback(k1, Dictionary.gridDictionary().MemberId, (x: String) => {
      logger.info("callback result is " + x)
      assert(x.equals("1"))
    })
    LogBroker.handleInfoLine(k1, "Group{Address=237.0.0.1, Port=9001, TTL=4}");
    LogBroker.handleInfoLine(k1, "");
    LogBroker.handleInfoLine(k1, "MasterMemberSet(");
    LogBroker.handleInfoLine(k1, "  ThisMember=Member(Id=1, Timestamp=2012-06-06 18:05:45.16, Address=192.168.100.7:8088, MachineId=3079, Location=site:Site1,process:50780, Role=CoherenceServer)");
    assert(LogBroker.subscribers.size == 0)
    //    assert(Dictionary.jvmDictionary.JavaCommand.value.endsWith("java"))
  }

  test("CallbackUnsubscribeingLogSubscriber not throw exception if valueRegexp doesn't match") {
    var k1 = new LaunchableKey("test", 1)
    var mockDictionaryEntry = new DictionaryEntry("some name", "ThisMember=Member(Id=1", Some("something not matching"))
    val s = LogBroker.subscribeCallback(k1, mockDictionaryEntry, (x: String) => {
      logger.info("callback result is " + x)
    })
    LogBroker.handleInfoLine(k1, "Group{Address=237.0.0.1, Port=9001, TTL=4}");
    LogBroker.handleInfoLine(k1, "");
    LogBroker.handleInfoLine(k1, "MasterMemberSet(");
    LogBroker.handleInfoLine(k1, "  ThisMember=Member(Id=1, Timestamp=2012-06-06 18:05:45.16, Address=192.168.100.7:8088, MachineId=3079, Location=site:Site1,process:50780, Role=CoherenceServer)");
    assert(LogBroker.subscribers.size == 0)
    //    assert(Dictionary.jvmDictionary.JavaCommand.value.endsWith("java"))
  }

  test("Update to of the gridProperty of the Launchable should be visible") {
    var k = new LaunchableKey("test", 1)
    var l = new JavaLaunchable(k, 0)
    LaunchableCache.putLaunchable(l)
    val gp = Dictionary.gridDictionary().MemberId
    val callback = (x: String) => LaunchableCache.setGridProperty(k, gp, x)

    val s = LogBroker.subscribeCallback(k, gp, callback)
    LogBroker.handleInfoLine(k, "Group{Address=237.0.0.1, Port=9001, TTL=4}");
    LogBroker.handleInfoLine(k, "");
    LogBroker.handleInfoLine(k, "MasterMemberSet(");
    LogBroker.handleInfoLine(k, "  ThisMember=Member(Id=1, Timestamp=2012-06-06 18:05:45.16, Address=192.168.100.7:8088, MachineId=3079, Location=site:Site1,process:50780, Role=CoherenceServer)");

    assert(LaunchableCache.get(k).getGridProperty(Dictionary.gridDictionary().MemberId) == "1")
    logger.info("%s".format(LaunchableCache.get(k)))

    assert(LogBroker.subscribers.size == 0)
    //    assert(Dictionary.jvmDictionary.JavaCommand.value.endsWith("java"))
  }

  test("two Logging keys with same line numbers and launchable keys should be equal") {

    val k1 = new LaunchableKey("test", 1)
    val k2 = new LaunchableKey("test", 1)

    val lk1 = new LoggingKey(0, k1)
    val lk2 = new LoggingKey(0, k1)

    assert(k1.equals(k2))
    assert(k2.equals(k1))

    assert(lk1.equals(lk2))
    assert(lk2.equals(lk1))
  }

  test("Log lines inserted in the logging cache should be retrieved by LaunchableKey") {
    val k = new LaunchableKey("test", 2)
    val l = "my log line"
    val lk = new LoggingKey(0, k)
    LoggingCache.put(lk, l)
    assert(LoggingCache.get(lk).equals(l))
    //assert(RemoteAPI.getLogs(lk).endsWith(l))
  }

  test("multithreaded allocation of grid ids should produce unique ids") {
    // println(UniqueGridId.nextId())
    var allIds = new HashSet[Long] with SynchronizedSet[Long]
    val threadCount = 100
    val iterations = 10000

    for (i <- 0 until threadCount) {
      AsyncInvoker.async({
        for (i <- 0 until iterations) {
          val nextId = LongGridSequence.nextId()
          if (allIds.contains(nextId)) {
            assert(false)
          } else {
            allIds += nextId
          }
        }
      })
    }

    logger.info("%s".format(LongGridSequence.nextId()))

    //FIXME - come up with proper assert
    while (allIds.size != threadCount * iterations) {
      logger.info("current size is " + allIds.size)
      logger.info(LongGridSequence.nextId().toString)
      Thread.sleep(1000)
    }
    logger.info("total size is " + allIds.size)
    assert(allIds.size == threadCount * iterations)
  }

  test("EventKey equals") {
    val e1 = new EventKey(2, "testtype", "test1", "test1", "test1" )
    val e2 = new EventKey(2, "testtype", "test2", "test2", "test2")
    assert(e1.equals(e1))
    assert(e1.equals(e2))
  }

  test("Test iterating over elements of dictionary ") {
    Dictionary.gridDictionary().productIterator.foreach(a => logger.info(a.asInstanceOf[DictionaryEntryable].lineMatch))
  }


  test("OSOperations.isValidPid") {
    val pid = "76448"
    val isValidPid = OSOperations.isValidPid(pid)
    logger.info("valid pid=%s = %s".format(pid, isValidPid))
  }
  
  test("OSOperations.getUserName") {
    val username = OSOperations.getUserName()
    logger.info("username=%s".format(username))
  }
  
  
  
  test("scheduling jobs") {

//    var v = org.quartz.JobBuilder.newJob(new OSHeartbeatInitiator().getClass())
//      .withIdentity("job1", "group1")
//      .build();
//    // Trigger the job to run now, and then every 40 seconds
//    var trigger = org.quartz.TriggerBuilder.newTrigger()
//      .withIdentity("trigger1", "group1")
//      .startNow()
//      .withSchedule(org.quartz.SimpleScheduleBuilder.simpleSchedule()
//        .withIntervalInSeconds(4)
//        .repeatForever())
//      .build();
//
//    JobScheduler.schedule(v, trigger)
    //  JobScheduler.unschedule(v.getKey())
//
//    HeartbeatCache.addOsHeartbeatListener(new MapListener() {
//      override def entryUpdated(e: MapEvent) { println(e) }
//      override def entryInserted(e: MapEvent) { println(e) }
//      override def entryDeleted(e: MapEvent) { println(e) }
//    })

  }
  
  test("config cache") {
    val v = ConfigCache.get("checks.properties", "os-checks-interval-sec")
    
    Test.testLister()
    
    Thread.sleep(10000);
    
    logger.info("os-checks-interval-sec = " +v)
  }

//  test("java command should concorporate JAVA_HOME") {
//
//    var s = new UnixSupport
//
//    s.env = s.env.toMap.filter(x => x._1.equals("JAVA_HOME"))
//    assert(s.getJavaCommand().equals("java"))
//
//    s.env = s.env ++ Map("JAVA_HOME" -> "/something")
//    assert(s.getJavaCommand().equals("/something/bin/java"))
//
//  }
  
   test("java command should concorporate JAVA_HOME") {
     println(OSOperations.getJavaCommand)

    assert(OSOperations.getJavaCommand.endsWith("java"))


  }
  test("ensure RemoteStartTask is serialized correctly") {

    val l = List(new LaunchableKey("a",1), new LaunchableKey("a",2))

    val t = new RemoteStartTask
    t.setKeys(l)

    //just any cache
    var c = CacheFactory.getCache("runbook");

    c.put("test", t)

     val t2 = c.get("test").asInstanceOf[RemoteStartTask]
     assert(t2.getLaunchableKeys().size == 2)

  }

  test("ensure RemoteStopTask is serialized correctly") {

    val l = List(new LaunchableKey("a",1), new LaunchableKey("a",2))
    val t = new RemoteStopTask
    t.setKeys(l)

    //just any cache
    var c = CacheFactory.getCache("runbook");

    c.put("test", t)

    val t2 = c.get("test").asInstanceOf[RemoteStopTask]
    assert(t2.getLaunchableKeys().size == 2)

  }

}