package org.gridvise.tests

import org.scalatest.FunSuite
import org.gridvise.logical.os.OSOperations
import org.slf4j.LoggerFactory

class TmpTests extends FunSuite {

  def logger = LoggerFactory.getLogger(this.getClass())

  test("extract pid cpu usage") {
    var v = OSOperations.getCPUUsage()
    logger.info("$s".format(v))
  }
  test("getCpu higer") {
    var v = OSOperations.getCPUUsageHigherThen(20)
    logger.info("%s".format(v))
  }

}