package org.gridvise.tests


import collection.JavaConversions._
import org.gridvise.xmlbindings._
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.gridvise.logical.runbook.activity.ActivityContextFactory
import scala.xml.XML
import org.gridvise.mgmtcache.coh.entity.runbook.RunBookCache
import org.gridvise.logical.runbook.LocalRunBook
import org.gridvise.mgmtcache.coh.entity.activityresult.ActivityResultCache
import org.gridvise.mgmtcache.coh.invocation.tasks.runbook.RunBookTask
import org.gridvise.{RemoteRunBookAPI, RemoteAPI}
import org.slf4j.LoggerFactory

@RunWith(classOf[JUnitRunner])
class RunBookTest extends FunSuite {

  def logger = LoggerFactory.getLogger(this.getClass())
    
  test("test perform runbook activity") {
    val runBookXML = XML.load(getClass().getClassLoader().getResourceAsStream("runBook.xml"))
    val runBook = scalaxb.fromXML[RunBook](runBookXML)
    val a = runBook.Activities.Activity.find(x => x.id.equals("HelloWorld"))
    assert(ActivityContextFactory.createActivityContext("testRunBook", a.get, Map("HE"->"hello", "WO" -> "world")).perform().equals("hello world!"))
  }

  test("test RunBook cache") {
    val runBookXML = XML.load(getClass().getClassLoader().getResourceAsStream("runBook.xml"))
    val runBook = scalaxb.fromXML[RunBook](runBookXML)
    RunBookCache.putRunBook(runBook)
    val cachedRunBook = RunBookCache.getRunBook("testRunBook")
    assert(cachedRunBook != null)

    val a = runBook.Activities.Activity.find(x => x.id.equals("HelloWorld"))
    a.get.value.equals("echo ${HE} ${WO}!")
  }

  test("run activity via local runbook"){

    ActivityResultCache.clearCache()

    LocalRunBook.loadRunBook("runBook.xml")

    val r1 = LocalRunBook.runActivity("testRunBook", "HelloWorld", Map("HE"->"hello", "WO" -> "world"))
    assert(r1.equals("hello world!"))
    val activityCollection = ActivityResultCache.getActivities("testRunBook", "HelloWorld")
    assert(activityCollection.size()==1)
    assert(activityCollection.head.result.equals("hello world!"))

  }

  test("run activity via remote api"){

    ActivityResultCache.clearCache()

    LocalRunBook.loadRunBook("runBook.xml")

    val r1 = RemoteRunBookAPI.invokeRunBookActivity("testRunBook", "HelloWorld", Map("HE"->"hello", "WO" -> "world"))
    assert(r1.equals("[hello world!]"))
    val activityCollection = ActivityResultCache.getActivities("testRunBook", "HelloWorld")
    assert(activityCollection.size()==1)
    assert(activityCollection.head.result.equals("hello world!"))

  }

  test("run book via remote api"){

    ActivityResultCache.clearCache()

    LocalRunBook.loadRunBook("runBook.xml")

    val r1 = RemoteRunBookAPI.invokeRunBook("testRunBook", Map("HE"->"hello", "WO" -> "world"))
    assert(r1.contains("HelloWorld=>hello world!"))
    assert(r1.contains("Task2=>"))
    assert(r1.indexOf("HelloWorld")<r1.indexOf("Task"))

    val activityCollection = ActivityResultCache.getActivities("testRunBook")
    assert(activityCollection.size()==2)
    assert(activityCollection.filter(p => p.result.equals("hello world!")).size == 1)
    assert(activityCollection.filter(p => p.result.equals("Task2")).size == 1)
  }

  test("test vars"){

    val rbt = new RunBookTask("test")
    rbt.setVars(Map("a"->"1", "b"->"2"))

    assert(rbt.getVars()("a").equals("1"))
    assert(rbt.getVars()("b").equals("2"))
  }


  test("remote api - machine names"){
    logger.info("%s".format(RemoteAPI.machineNames()))
  }

}