package org.gridvise.tests

import scala.collection.mutable.HashSet
import scala.collection.mutable.Stack
import scala.collection.mutable.SynchronizedSet
import org.gridvise.logical.gridsequence.LongGridSequence
import org.gridvise.logical.logbroker.LogBroker
import org.gridvise.logical.os.OSOperations
import org.gridvise.logical.{CmdLineLaunchable, ClusterConfigParser, JavaLaunchable}
import org.gridvise.mgmtcache.coh.entity.config.ConfigCache
import org.gridvise.mgmtcache.coh.entity.events.EventKey
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableCache
import org.gridvise.mgmtcache.coh.entity.launchable.LaunchableKey
import org.gridvise.mgmtcache.coh.entity.logging.LoggingCache
import org.gridvise.mgmtcache.coh.entity.logging.LoggingKey
import org.gridvise.util.jmx.Test
import org.gridvise.util.AsyncInvoker
import org.gridvise.util.Dictionary
import org.gridvise.xmlbindings._
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.gridvise.mgmtcache.coh.invocation.tasks.{RemoteStopTask, RemoteStartTask}
import com.tangosol.net.CacheFactory
import org.gridvise.xmlbindings.DictionaryEntry
import scala.Some
import org.slf4j.LoggerFactory
import scala.collection.JavaConversions._
import scala.sys.process._
import scala.sys.process.stringSeqToProcess

@RunWith(classOf[JUnitRunner])
class LaunchableTest extends FunSuite {
  
  def LOG = LoggerFactory.getLogger(this.getClass)

  test("test creation of Launchables") {
    val testClassesDir = getClass().getProtectionDomain().getCodeSource().getLocation().getFile()

    val xml = io.Source.fromInputStream(getClass.getClassLoader.getResourceAsStream("sample-jvms.xml")).mkString
    LOG.info("testing creation of JVMs from "+xml)



    System.setProperty("TEST_HOME", testClassesDir+"../..")

    ClusterConfigParser.initialize(xml)

    val ls = LaunchableCache.getLaunchablesForJvmConfig("cmdTest")

    LOG.info("retrieved "+ls.size()+" launchables for cmdTest")

    ls.toList.foreach(lk => {
      val l = LaunchableCache.get(lk)
      val p = l.asInstanceOf[CmdLineLaunchable].getProcessIdentifier(null)
      println("test pid "+p)
      assert(p.equals("12345"+l.ordinal))

      val s = l.start()
      LOG.info("start = "+s)
    })


  }


}