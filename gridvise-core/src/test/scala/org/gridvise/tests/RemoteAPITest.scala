package org.gridvise.tests

import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import org.gridvise.RemoteAPI
import org.slf4j.LoggerFactory
import scala.sys.process._
import org.scalatest._

@RunWith(classOf[JUnitRunner])
class RemoteAPITest extends FunSuite {

  def logger = LoggerFactory.getLogger(this.getClass())
    
  test("test machine name") {

    val machineNames = RemoteAPI.machineNames()
    logger.info("machine names "+machineNames)
    logger.info("hostname"!!)

    val realHostname = "hostname"!!

    //assert(machineNames.size() == 1, "one machine expected")
    //assert(machineNames(1).startsWith(realHostname), "machine names must match")

  }


}