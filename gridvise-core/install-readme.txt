- advisable plugins
	+ m2 plugin
	+ scala plugin
	
expose system variables on osx in etc/launchd.conf
setenv MVN_REPO /Users/christoph/.m2/repository
setenv COHERENCE_HOME /Users/christoph/Dropbox/apps/coherence/coherence-java-3.7.1.0b27797/coherence
setenv LOG_HOME /Users/christoph/Documents/workspace/gridvise-util/gridvise-util
setenv APP_HOME /Users/christoph/Documents/workspace/gridvise-util/gridvise-util

change magment config to reflect hostname

launch config scala ext tool